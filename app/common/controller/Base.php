<?php
namespace app\common\controller;

use \thans\Ret;
use \think\Controller;
use \think\Loader;
use \think\Request;
use \think\Session;

/**
 *
 */
class Base extends Controller
{

    public $now_uri;
    public $now_url;
    public $ret;
    public function __construct()
    {
        $this->request = Request::instance();
        parent::__construct();

        $this->now_uri = $this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action();
        $this->now_uri = strtolower($this->now_uri);
        // var_dump($_GET);
        $this->now_url = $this->request->path();
        // var_dump($param);
        $this->assign('base_uri', url($this->now_uri));
        $this->assign('now_uri', $this->now_uri);
        $this->assign('now_url', $this->now_url);
        //$this->now_url=$this->request->url();

        // $this->checkAuth();
        $this->ret = Ret::i();
        /**
         * 请求类型
         * @var [type]
         */
        $this->request_type = $this->get_request();

        define('IS_POST',$this->request_type=='POST'?true:false);

        $this->log($this->request_type);
        $this->options = Loader::model('options');
        /*
        自动从数据库中加载配置项。
         */
        $this->auto_load_options();
    }

    /**
     * 记录操作日志
     */
    public function log($retype)
    {
        $data['log_type'] = $retype;

        $log = Loader::model('ActionsLog');

        //根据rule_value 获取菜单名称
        $rule_value       = strtolower($this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action());
        $data['log_name'] = Loader::model('Menu')->where('rule_value', $rule_value)->find()['name'];
        // var_dump($data);die();
        if ($rule_value == "home/index/main") {
            return;
        }
        $data['log_m']     = $this->request->module();
        $data['log_c']     = $this->request->controller();
        $data['log_a']     = $this->request->action();
        $data['log_value'] = $this->now_url;
        
        $data['log_time'] = date('Y-m-d H:i:s', time());

        $data['log_uid'] = intval(session('u_id'));

        $data['log_ip'] = inge_get_ip();

        $data['log_os']      = inge_get_os();
        $data['log_browser'] = inge_get_browser() . ' ' . inge_get_browser_ver();

        $body['header']   = $this->request->header();
        // $body['body']     = input('');
        // $data['log_body'] = json_encode($body);
        $log->insert($data);
    }
    public function auto_load_options()
    {
        $res = $this->options->where('option_autoload', '1')->select();
        foreach ($res as $key => $value) {
            $options = json_decode($value['option_value'], true);
            if (is_array($options)) {
                foreach ($options as $k => $v) {
                    $this->assign($k, $v);
                }
            } else {
                $this->assign($value['option_name'], $options);
            }
        }
    }
    /**
     * 获取请求类型
     * @return [type] [description]
     */
    public function get_request()
    {
        if (Request::instance()->isGet()) {
            return 'GET';
        }
        if (Request::instance()->isPost()) {
           return 'POST';   
        }
        if (Request::instance()->isPut()) {
           return 'PUT';
        }
        if (Request::instance()->isAjax()) {
            return 'Ajax';
        }
        if (Request::instance()->isPjax()) {
            return 'Pjax';
        }
        if (Request::instance()->isHead()) {
            return 'HEAD';
        }
        if (Request::instance()->isPatch()) {
            return 'PATCH';
        }
        if (Request::instance()->isCli()) {
            return 'cli';
        }
        if (Request::instance()->isCgi()) {
            return 'cgi';
        }
    }

}
