<?php
namespace app\common\controller;

use \app\common\controller\Base;
use \think\Config;
use \think\Loader;
use \think\Session;

/**
 *
 */
class Homebase extends Base
{

    public $user   = null;
    public $userId = 0;

    public function __construct()
    {

        parent::__construct();
        $this->initializeCheck();
        $this->checkAuth();
    }

    /**
     * @name 初始化加载
     * @在构造函数中验证
     */
    protected function initializeCheck()
    {
        $user   = Loader::model('user');
        $userId = intval(session('u_id'));
        if ($userId) {
            $user_info = $user->getUserByUserId($userId);
            if ($user_info) {
                if ($user_info->status !== 1) {
                    switch ($user_info->status) {
                        case '0':
                            $this->error("您的账户已被封禁！", url('/admin/login.html'));
                            break;
                        default:
                            $this->error("您的账户异常！", url('/admin/login.html'));
                            break;
                    }
                } else if ($user_info->is_del === 1) {
                    $this->error("账户不存在", url('admin/login'));
                } else {
                    $u_info = $user_info->toArray();
                    $this->user   = $user_info;
                    unset($u_info['user_pwd']);
                    unset($u_info['user_pwd_rand']);
                    session('u_info', $u_info);
                    // var_dump($u_info);die();
                    if ($u_info['is_del'] == 1 || $u_info['status'] != 1) {
                        return "\"<script>top.location.href=\"/admin/login.html\"</script>\"";
                        die;
                    }
                    $this->assign('u_info', $u_info);
                    $this->userId = $user_info->user_id;
                    return true;
                }
            }
        }
        $this->error("未登录", url('/admin/login.html?jumpurl='.urlencode($this->now_url)));
    }

    /**
     * @name 权限验证 （强制验证）
     */
    protected function checkAuth()
    {
        // var_dump($this->user);
        $u_auth = $this->user->getAuth()->where('auth_id', session('u_info')['auth_id'])->find();
        $this->assign('u_auth', $u_auth);
        //  判断该地址是否在权限验证过滤地址中
        $filter_uri = Config::get('auth.filter_uri');
        if (in_array($this->now_url, $filter_uri)) {
            return $this->user;
        }
        //  判断该用户是否为超管权限
        $admin_auth_id = Config::get('auth.admin_auth_id');
        if (isset($this->user->user_id)) {
            if ($this->user->user_id == $admin_auth_id) {
                return $this->user;
            }
        }
        //  判断该用户是否存在权限过滤用户中
        $filter_u_id = Config::get('auth.filter_u_id');
        if (isset($this->user->user_id)) {
            if (in_array($this->user->user_id, $filter_u_id)) {
                return $this->user;
            }
        }

        //  先判断菜单地址中该权限是否需要权限验证
        $auth_rule = Loader::model('AuthRule');
        // var_dump(Request::instance()->path());
        $rule = $auth_rule->where('rule_name', $this->now_uri)->find();
        // var_dump($menu);
        // die();
        //  没有找到该地址默认为不验证权限
        if (!$rule) {
            return $this->user;
        }
        // var_dump($menu['data']);
        // 判断是否有参数需要判断，如果没有的话则直接验证rule_value 有的话传递需要验证的参数进行验证
        $data = '';
        if ($rule['rule_param']) {
            $param = explode('/', $rule['rule_param']);
            foreach ($param as $key => $value) {
                if ($key % 2 == 0) {
                    if ($data) {
                        $data .= '/' . $value . '/' . input($value);
                    } else {
                        $data .= $value . '/' . input($value);
                    }
                }
            }
        }
        // var_dump($data);
        //  查询该地址是否存在权限记录中
        $authRecord = $this->user->getAuthRecordOneByRulValue($this->now_uri, $data);
        // var_dump($this->user);die();
        if (!$authRecord) {
            $this->error("您没有该项权限", url('/home/index/main'));
            die;
        }
        return $this->user;
    }

}
