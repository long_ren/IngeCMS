<?php
namespace app\common\model;

use think\Model;

use \app\common\model\AuthRecord;
use \app\common\model\Menu;

class Auth extends Model
{
    public function getAuthRecordByRulValue($rule_value)
    {
        $authRecord = AuthRecord::where('auth_id',$this->auth_id)->where('rule_value',$rule_value)->find();
        return $authRecord;
    }
    public function getAuthRecordList()
    {
        if($this->auth_id == 1){
            $authRecord = Menu::where('status',1)->field('id,menu_pid,name,listorder,icon,rule_value,data')->order('listorder ASC')->select();
        }else{
            $authRecord = AuthRecord::where('a.auth_id',$this->auth_id)->alias('a')->join('menu m','(a.rule_value = m.rule_value and m.status = 1)')->field('m.id,m.menu_pid,m.name,m.listorder,m.icon,a.rule_value,m.data')->order('m.listorder ASC')->select();
            $authRecord2 = Menu::where('status',1)->where('type','0')->field('id,menu_pid,name,listorder,icon,rule_value,data')->order('listorder ASC')->select();
            $authRecord = array_merge($authRecord,$authRecord2);
        }
        foreach ($authRecord as $key => $value) {
            $authRecord[$key]['rule_value']=$value['data'] ? $value['rule_value'].'/'.$value['data'] : $value['rule_value'];
        }
        // die();
//        echo AuthRecord::getLastSql();die;
        return $authRecord;
    }
}