<?php
namespace app\common\model;

use think\Model;
use \think\Db;
use \think\Loader;
use \think\Validate;

class GoodsAttribute extends Model
{
	public function checkAttr($data)
	{	
		$rules = [
						'attr_name|商品属性名称'  => 'require',
						'mould_id|商品模型'=> 'require',
						];
		$msg = [
						'attr_name.require' => '商品属性名称不能为空',
						'mould_id.require'=> '商品模型不能为空',
						];
		$validate=new Validate($rules,$msg);
		$result   = $validate->check($data);
		if (!$result) {
				$msg['code'] = 0;
				$msg['msg']  = $validate->getError();
				return $msg;
			} else {
				return false;
			}
	}    
	
	public function saveAttr($data, $id = '', $allowField = true)
    {
        $res = $this->checkAttr($data);
        if ($res) {
            return $res;
        }

        $attr['mould_id']      = $data['mould_id'];
        $attr['attr_name']      = $data['attr_name'];
        $attr['listorder']   = $data['listorder'];
        $attr['attr_way']= $data['attr_way'];
        if ($data['attr_way']==1){
        	if(empty($data['attr_value'])){
        		$msg['code'] = '0';
        		$msg['msg']  = '当前选择框要输入值';
        		return $msg;
        	}
        	$data['attr_value']=explode("\n", $data['attr_value']);
        	foreach ($data['attr_value'] as $ki=>$vue){
        		$data['attr_value'][$ki] =  str_replace("\r","",$data['attr_value'][$ki]);
        	}
        $attr['attr_value']=json_encode($data['attr_value']);
        }
         if ($id) {
         	$msg = $this->checkName($attr['mould_id'],$attr['attr_name']);
         	if ($msg){
         		return $msg;
         	}
             $update = $this->where('mould_id', $id)->where($attr)->find();
	         if (!$update) {
	                   $update = $this->where('mould_id', $id)->update($attr);
	                    if($update){
	                    	$msg['code'] = '1';
	                    	$msg['msg']  = '更新成功';
	                    	return $msg;
	                }
         		}
         		$msg['code'] = '1';
         		$msg['msg']  = '没有更改';
         		return $msg;
         }
        $msg = $this->checkName($attr['mould_id'],$attr['attr_name']);
         if ($msg){
         	return $msg;
         }
         $res   = $this->insertGetId($attr);
         if ($res){
         	$msg['code'] = '1';
         	$msg['msg']  = '保存成功';
         	return $msg;
         }
         $msg['code'] = '0';
         $msg['msg']  = '保存失败';
         return $msg;
    }
    
    /**
     * 检验次父类下有没有同名属性名
     * @date: 2017年4月6日 下午1:15:30
     * @author: 姚荣
     * @return:错误返回$msg
     */
    private function checkName($pid,$name){
    	$getFormat = $this->where('mould_id', $pid)->where('attr_name',$name)->find();
    	if ($getFormat){
    		$msg['code'] = '0';
    		$msg['msg']  = '相同模型下已经有此属性';
    		return $msg;
    	}
    }
}
