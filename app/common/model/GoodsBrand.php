<?php
namespace app\common\model;

use think\Model;
use \think\Db;
use \think\Loader;
use \think\Validate;

class GoodsBrand extends Model
{
	public function checkBrand($data)
	{	
		$rules = [
						'brand_name|商品品牌名称'  => 'require',
						'brand_url|商品品牌地址'  => 'url|max:250',
						'logo|商品品牌LOGO'  => 'max:150',
						'brand_desc|商品品牌描述' => 'max:250'
						];
		$msg = [
						'brand_name.require' => '商品品牌名称不能为空',
						'brand_url.url' => '商品品牌地址是非法URL',
						'brand_url.max' => '商品品牌地址超过最大值',
						'logo.max' => '商品品牌LOGO超过最大值',
						'brand_desc.max' => '商品品牌描述超过最大值',
						];
		$validate=new Validate($rules,$msg);
		$result   = $validate->check($data);
		if (!$result) {
				$msg['code'] = 0;
				$msg['msg']  = $validate->getError();
				return $msg;
			} else {
				return false;
			}
	}    
	
	public function saveBrand($data, $id = '', $allowField = true)
    {
        $res = $this->checkBrand($data);
        if ($res) {
            return $res;
        }
        $brand['cate_id']       = isset($data['cate_id'])?$data['cate_id']:0;
        $brand['brand_name']      = $data['brand_name'];
        $brand['brand_url']        = $data['brand_url'];
        $brand['logo']  = $data['logo'];
        $brand['listorder']   = $data['listorder'];
        $brand['brand_desc']= $data['brand_desc'];
         if ($id) {
         	$msg = $this->checkName($brand['cate_id'],$brand['brand_name']);
         	if ($msg){
         		return $msg;
         	}
             $update = $this->where('brand_id', $id)->where($brand)->find();
	         if (!$update) {
	                   $update = $this->where('brand_id', $id)->update($brand);
	                    if($update){
	                    	$msg['code'] = '1';
	                    	$msg['msg']  = '更新成功';
	                    	return $msg;
	                }
         		}
         		$msg['code'] = '1';
         		$msg['msg']  = '没有更改';
         		return $msg;
         }
         $msg = $this->checkName($brand['cate_id'],$brand['brand_name']);
         if ($msg){
         	return $msg;
         }
         $res   = $this->insertGetId($brand);
         if ($res){
         	$msg['code'] = '1';
         	$msg['msg']  = '保存成功';
         	return $msg;
         }
         $msg['code'] = '0';
         $msg['msg']  = '保存失败';
         return $msg;
    }
    /**
     * 检验次父类下有没有同名品牌名
     * @date: 2017年4月6日 下午1:15:30
     * @author: 姚荣
     * @return:错误返回$msg
     */
    private function checkName($pid,$name){
    	$getBrand = $this->where('cate_id', $pid)->where('brand_name',$name)->find();
    	if ($getBrand){
    		$msg['code'] = '0';
    		$msg['msg']  = '相同分类下已经有此品牌';
    		return $msg;
    	}
    }
    
    public function delBrand($id)
    {
    	$map['brand_id']  = $id;
    	$child=$this->where($map)->find();
    	if ($child){
    		$res2=$this->where($map)->delete();
    		$res=true;
    	}else{
    		$res=false;
    	}
    	return $res;
    }
    
}
