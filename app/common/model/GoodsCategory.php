<?php
namespace app\common\model;

use think\Model;
use \think\Validate;

class GoodsCategory extends Model
{
    public function checkForm($data)
    {
        $rules = [
            'name|商品分类名称' => 'require',
        ];
        $msg = [
            'name.require' => '分类名称不能为空',
        ];
        $validate = new Validate($rules, $msg);
        $result = $validate->check($data);
        if (!$result) {
            $msg['code'] = 0;
            $msg['msg'] = $validate->getError();
            return $msg;
        } else {
            return false;
        }
    }

    public function saveCategory($data, $id = '', $allowField = true)
    {
        //暂时只可能三级分类
        if ($this->getCatLevel($data['category_pid']) == 3) {
            $msg['code'] = '0';
            $msg['msg'] = '最多只可以三级分类';
            return $msg;
        }
        $res = $this->checkForm($data);
        if ($res) {
            return $res;
        }
        $category['category_pid'] = isset($data['category_pid']) ? $data['category_pid'] : 0;
        $category['name'] = $data['name'];
        $category['mobile_name'] = $data['mobile_name'];
        $category['level'] = $this->getCatLevel($data['category_pid']) + 1;
        $category['is_show'] = isset($data['is_show']) ? $data['is_show'] : '0';
        $category['is_hot'] = isset($data['is_hot']) ? $data['is_hot'] : '0';
        $category['listorder'] = $data['listorder'];
        $category['image'] = $data['image'];
        if ($id) {
            $update = $this->allowField($allowField)->where('category_id', $id)->where($category)->find();
            if (!$update) {
                $update = $this->allowField($allowField)->where('category_id', $id)->update($category);
                if ($update) {
                    $msg['code'] = '1';
                    $msg['msg'] = '更新成功';
                    return $msg;
                }
            }
        }
        $res = $this->allowField($allowField)->insertGetId($category);
        if ($res) {
            $msg['code'] = '1';
            $msg['msg'] = '保存成功';
            return $msg;
        }
        $msg['code'] = '0';
        $msg['msg'] = '保存失败';
        return $msg;
    }

    public function delCate($id)
    {
        $map['category_id'] = $id;
        $map['category_pid'] = $id;
        $child = $this->where('category_pid', $map['category_pid'])->find();
        if ($child) {
            $res1 = $this->where('category_pid', $map['category_pid'])->delete();
            $res2 = $this->where('category_id', $map['category_id'])->delete();
            if ($res1 && $res2) {
                $res = true;
            } else {
                $res = false;
            }
        } else {
            $res = $this->where('category_id', $map['category_id'])->delete();
            if ($res) {
                $res = true;
            } else {
                $res = false;
            }
        }
        return $res;
    }

    /**
     * 获取商品一级分类
     * @return [type] [description]
     */
    public function getFirstCat()
    {
        $list = $this->where('category_pid', 0)->select();
        return $list;
    }

    /**
     * 根据分类ID获取分类等级
     * @param  [type] $catid [description]
     * @return [type]        [description]
     */
    public function getCatLevel($catid)
    {
        $res = $this->where('category_id', $catid)->find();
        if ($res && $res['category_pid'] == 0) {
            return 1;
        } elseif ($res['category_pid'] != 0) {
            $level = 1;
            while ($res['category_pid'] != 0) {
                $res = $this->where('category_id', $res['category_pid'])->find();
                $level++;
            }
            return $level;
        } else {
            return 0;
        }
    }

    /**
     * 获取下级所有分类
     * @param  [type] $catid [description]
     * @return [type]        [description]
     */
    public function getLowerLevelCat($catid)
    {
        return $this->where('category_pid', $catid)->field('category_id as id,name as text')->select();
    }
}