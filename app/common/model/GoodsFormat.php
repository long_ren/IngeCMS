<?php
namespace app\common\model;

use think\Model;
use \think\Db;
use \think\Loader;
use \think\Validate;

class GoodsFormat extends Model
{
    public function checkFormat($data)
    {
        $rules = [
            'format_name|商品规格名称' => 'require',
            'cate_id|商品分类' => 'require',
            'format_desc|商品规格描述' => 'max:250'
        ];
        $msg = [
            'format_name.require' => '商品规格名称不能为空',
            'cate_id.require' => '商品分类不能为空',
            'format_desc.max' => '商品规格描述超过最大值',
        ];
        $validate = new Validate($rules, $msg);
        $result = $validate->check($data);
        if (!$result) {
            $msg['code'] = 0;
            $msg['msg'] = $validate->getError();
            return $msg;
        } else {
            return false;
        }
    }

    public function saveFormat($data, $id = '', $allowField = true)
    {
        $res = $this->checkFormat($data);
        if ($res) {
            return $res;
        }
        $format['cate_id'] = $data['cate_id'];
        $format['format_name'] = $data['format_name'];
        $format['listorder'] = $data['listorder'];
        $format['format_desc'] = $data['format_desc'];
        if ($id) {
            $msg = $this->checkName($format['cate_id'], $format['format_name']);
            if ($msg) {
                return $msg;
            }
            $update = $this->where('format_id', $id)->where($format)->find();
            if (!$update) {
                $update = $this->where('format_id', $id)->update($format);
                if ($update) {
                    $msg['code'] = '1';
                    $msg['msg'] = '更新成功';
                    return $msg;
                }
            }
            $msg['code'] = '1';
            $msg['msg'] = '没有更改';
            return $msg;
        }
        $msg = $this->checkName($format['cate_id'], $format['format_name']);
        if ($msg) {
            return $msg;
        }
        $res = $this->insertGetId($format);
        if ($res) {
            $msg['code'] = '1';
            $msg['msg'] = '保存成功';
            return $msg;
        }
        $msg['code'] = '0';
        $msg['msg'] = '保存失败';
        return $msg;
    }

    /**
     * 检验次父类下有没有同名规格名
     * @date: 2017年4月6日 下午1:15:30
     * @author: 姚荣
     * @return:错误返回$msg
     */
    private function checkName($pid, $name)
    {
        $getFormat = $this->where('cate_id', $pid)->where('format_name', $name)->find();
        if ($getFormat) {
            $msg['code'] = '0';
            $msg['msg'] = '相同分类下已经有此规格';
            return $msg;
        }
    }

    public function delFormat($id)
    {
        $recycleBin           = Loader::model('RecycleBin');
        $map['format_id']        = $id;
        $data                 = $this->where($map)->find();
        $res                  = $recycleBin->cutout($this, $data, $map);
        return $res;
    }
}
