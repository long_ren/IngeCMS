<?php
namespace app\common\model;

use think\Model;
use think\Db;
use think\Loader;
use think\Validate;

class GoodsMould extends Model
{
    public function checkMould($data)
    {
        $rules = [
            'mould_name|商品模型名称' => 'require',
            'mould_desc|商品模型描述' => 'max:255',
            'cate_id|商品所属模型' => 'require',
        ];
        $msg = [
            'mould_name.require' => '商品模型名称不能为空',
            'mould_desc.max' => '商品模型描述超过最大值',
            'cate_id' => '商品所属模型不能为空',
        ];
        $validate = new Validate($rules, $msg);
        $result = $validate->check($data);
        if (!$result) {
            $msg['code'] = 0;
            $msg['msg'] = $validate->getError();
            return $msg;
        } else {
            return false;
        }
    }

    public function saveMould($data, $id = '', $allowField = true)
    {
        $res = $this->checkMould($data);
        if ($res) {
            return $res;
        }

        $mould['mould_desc'] = $data['mould_desc'];
        $mould['mould_name'] = $data['mould_name'];
        $mould['listorder'] = $data['listorder'];
        if (!empty($data['mould_brand'])) {
            $mould['mould_brand'] = json_encode($data['mould_brand']);
        } else {
            $mould['mould_brand'] = '';
        }
        if (!empty($data['mould_format'])) {
            $mould['mould_format'] = json_encode($data['mould_format']);
        } else {
            $mould['mould_format'] = '';
        }
        $mould['cate_id'] = $data['cate_id'];
        //更新操作
        if ($id) {
            $msg = $this->where('mould_name', $mould['mould_name'])->where('mould_id', '<>', $id)->find();
            if ($msg) {
                $msg['code'] = '0';
                $msg['msg'] = '有同名商品模型';
                return $msg;
            }
            $update = $this->where('mould_id', $id)->update($mould);
            if ($update) {
                $msg['code'] = '1';
                $msg['msg'] = '修改成功';
                return $msg;
            }
            $msg['code'] = '1';
            $msg['msg'] = '没有更改';
            return $msg;
        }
        //添加操作
        $msg = $this->where('mould_name', $mould['mould_name'])->find();
        if ($msg) {
            $msg['code'] = '0';
            $msg['msg'] = '有同名商品模型';
            return $msg;
        }
        $res = $this->insertGetId($mould);
        if ($res) {
            $msg['code'] = '1';
            $msg['msg'] = '保存成功';
            return $msg;
        }
        $msg['code'] = '0';
        $msg['msg'] = '保存失败';
        return $msg;
    }

    public function delMould($id)
    {
        $recycleBin = Loader::model('RecycleBin');
        $map['mould_id'] = $id;
        $data = $this->where($map)->find();
        $res = $recycleBin->cutout($this, $data, $map);
        return $res;
    }

}
