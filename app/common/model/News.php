<?php
namespace app\common\model;

use think\Model;
use \think\Db;
use \think\Loader;
use \think\Validate;
use thans\Common\Oss;

class News extends Model
{
    public function checkForm($data)
    {
        $validate = validate('News');
        $res      = $validate->check($data);
        if (!$res) {
            return $validate->getError();
        } else {
            return false;
        }
    }
    public function saveNew($data, $id = '', $allowField = true)
    {
        $res = $this->checkForm($data);
        if ($res) {
            $msg['code'] = '0';
            $msg['msg']  = $res;
            return $msg;
        }
        $new['new_flag']       = isset($data['new_flag']) ? json_encode($data['new_flag']) : '';
        $new['new_title']      = $data['new_title'];
        $new['new_titleshort'] = $data['new_titleshort'];
        $new['new_key']        = $data['new_key'];
        $new['new_author']     = session('u_id');
        $new['new_content']    = isset($data['editorValue'])?$data['editorValue']:'';
        $new['new_scontent']   = $data['new_scontent'];
        $new['new_zaddress']   = $data['new_zaddress'];
        $new['new_hits']       = 0;
        $new['new_like']       = 0;
        $new['new_img']= $data['new_img'];
        if (!$id) {
            $new['new_time'] = date('Y-m-d H:i:s', time());
        } else {
            $new['new_updatetime'] = date('Y-m-d H:i:s', time());
        }
        $new['listorder']    = $data['listorder'];
        $new['new_showtime'] = isset($data['new_showtime']) ? $data['new_showtime'] : date('Y-m-d H:i:s', time());
        $new['is_del']       = 0;
        $new['new_status']   = isset($data['new_status']) ? 1 : 0;
        Db::startTrans();
        try {
            if ($id) {
                $update = $this->where('new_id', $id)->where($new)->find();
                if (!$update) {
                    $update = $this->where('new_id', $id)->update($new);
                }
                $term['term_id']   = input('term_id');
                $TermRelationships = Loader::model('TermRelationships');
                if ($TermRelationships->where('object_id', $id)->find()['term_id'] != $term['term_id']) {
                    $res = $TermRelationships->where('object_id', $id)->update($term);
                } else {
                    $res = true;
                }
            } else {
                $res               = $this->insertGetId($new);
                $term['term_id']   = input('term_id');
                $term['object_id'] = $res;
                $update            = Loader::model('TermRelationships')->insert($term);
            }
            if ($update && $res) {
                Db::commit();
                $msg['code'] = '1';
                $msg['msg']  = '保存成功';
                return $msg;
            } else {
                Db::rollback();
                $msg['code'] = '0';
                $msg['msg']  = '保存失败';
                return $msg;
            }
            // 提交事务
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $msg['code'] = '0';
            $msg['msg']  = '保存失败2';
            return $msg;
        }
    }
    public function delNew($id)
    {
        $recycleBin           = Loader::model('RecycleBin');
        $map['new_id']        = $id;
        $termMap['object_id'] = $id;
        $data                 = $this->where($map)->find();
        $termData             = Loader::model('TermRelationships')->where($termMap)->find();
        $res                  = $recycleBin->cutout($this, $data, $map);
        if ($res && $termData) {
            $termRes = $recycleBin->cutout(Loader::model('TermRelationships'), $termData, $termMap);
        }
        return $res;
    }
}
