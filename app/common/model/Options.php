<?php
namespace app\common\model;

use think\Model;

class Options extends Model
{
    /**
     * 更新或新增配置
     * @param  [type] $title     [配置标题]
     * @param  [type] $value     [配置内容]
     * @param  [type] $auto_load [是否自动加载]
     * @return [type]            [description]
     */
    public function saveOptions($title, $value, $auto_load = 0)
    {
        $data['option_value']    = json_encode($value);
        $data['option_autoload'] = $auto_load;
        $data['option_name']     = $title;
        $res                     = $this->where('option_name', $title)->find();
        if ($res) {
            $res = $this->where('option_name', $title)->update($data);
        } else {
            $res = $this->insertGetId($data);
        }
        if ($res) {
        	$msg['code']='1';
        	$msg['msg']='配置更新成功';
            return $msg;
        } else {
            $msg['code']='0';
        	$msg['msg']='配置更新失败';
            return $msg;
        }
    }
}
