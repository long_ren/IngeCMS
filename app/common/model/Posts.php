<?php
namespace app\common\model;

use think\Model;

class Posts extends Model
{
	//获取模型的标识 作为data的post_type
	public function getPostType($id)
	{
		$map['post_type']='input_type';
		$map['post_id']=$id;
		$res = json_decode($this->where($map)->field('post_remark')->find()['post_remark'],true);
		return $res['post_mark'];
	}

}
