<?php
namespace app\common\model;

use think\Model;
use \think\Validate;
use \think\Request;
use \think\Db;

class Recharge extends Model{
	
	public function saveRecharge($user,$recharge) {
	    		//开启事务提交数据
    		Db::startTrans();{
    			try{
    				Db::name('user')->where('user_id',$user['user_id'])->find();
    				Db::name('user')->where('user_id',$user['user_id'])->setInc('coin',$user['coin']);	
    				Db::name('user')->where('user_id',$user['user_id'])->setInc('score',$user['score']);
    				Db::name('recharge')->insert($recharge);
    				Db::commit();
    			}catch (\Exception $e) {
				    // 回滚事务
				    Db::rollback();
				    return false;
				}
    		}
    		return true;
    	}
}