<?php
namespace app\common\model;

use think\Model;
use \think\Validate;
use \think\Request;
use \think\Db;

class RecycleBin extends Model
{
    //将数据剪切到回收站
    /**
     * @param $orm      数据库对象
     * @param $ormData  查询后的数据
     * @param $where    查询条件
     * @return bool     true:成功  false:失败
     */
    public function cutout($orm,$ormData,$where){
        $tableName = $orm->db()->getTable();
        $tableData =is_null($ormData)?array():$ormData->toArray();

        $data['bin_table_name'] = $tableName;
        $data['bin_values'] = json_encode($tableData);
        $data['bin_add_time'] = date("Y-m-d H:i:s", time());
//        var_dump($where);
        Db::startTrans();
        try{
            if($orm->where($where)->find()){
                $res = $orm->where($where)->delete();
            }else{
                $res = true;
            }
//        var_dump($res);
            if($res) {
                $id = $this->insertGetId($data);
                Db::commit();
                return true;
            } else {
                Db::rollback();
                return false;
            }
        }catch (\Exception $e) {
            Db::rollback();
            return false;
        }
    }
}
