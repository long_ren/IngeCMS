<?php
namespace app\common\model;

use think\Model;
use \think\Validate;
use \think\Db;

class Stores extends Model
{
	public function checkForm($data){
		$validate = validate('Stores');
		$res = $validate->check($data);
		if (!$res) {
			return $validate->getError();
		} else {
			return false;
		}
	}
	public function saveStore($data,$id='')
	{
		if($id){
			$res1 = $this->where('store_name',$data['store_name'])->where('store_id','<>',$id)->find();
			if($res1){
				$msg['code']='0';
				$msg['msg']='店铺名重复';
				return $msg;
			}
			$res2 = $this->where('store_id',$id)->find();
			if(!$res2){
				$msg['code']='0';
				$msg['msg']='店铺不存在';
				return $msg;
			}
			$res3 = $this->save($data,['store_id'=>$id]);
			if($res3){
				$msg['code']='1';
				$msg['msg']='店铺更新成功';
				return $msg;
			}
		}else{
			$res = $this->where('store_name',$data['store_name'])->find();
			if($res){
				$msg['code']='0';
				$msg['msg']='店铺名重复';
				return $msg;
			}
			$res = $this->insertGetId($data);
		}
		if($res){
			$msg['code']='1';
			$msg['msg']='保存成功';
		}else{
			$msg['code']='0';
			$msg['msg']='保存失败';
		}
		return $msg;
	}
	
		public function getStoreByName($store_name)
		{
			return $this->getStorerBy('store_name', $store_name);
		}
		
		public function getStoreById($id)
		{
			return $this->getStoreBy('store_id', $id);
		}
		
		public function getStoreByUserId($id)
		{
			$map['user_id'] = $id;
			return $this->where($map)->find();
		}
		
		public function getStoreBy($filed, $value)
		{
			$map[$filed] = $value;
			$store_info   = $this->where($map)->find();
			if ($store_info) {
				return $store_info->toArray();
			} else {
				return false;
			}
		}
}