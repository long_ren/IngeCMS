<?php
namespace app\common\validate;

use think\Validate;

class News extends Validate
{
    protected $rule = [
        'term_id|文章分类'       => 'require|number',
        'new_title|文章标题'     => 'require|max:255|min:1',
    	'new_titleshort|副标题' => 'max:200',
        'new_flag|文章属性'     => 'max:200|checkFlag:new_flag',
        'new_key|关键词'        => 'max:255',
        'new_zaddress|跳转地址'        => 'url',
        'new_showtime|显示时间'        => 'date',
        'new_scontent|简介'    => 'max:255',
    ];

    protected $message = [
        'term_id.require'    => '必须选择分类',
        'term_id.number'     => '非法操作',
        'new_title.require'  => '文章标题不能为空',
        'new_title.max'      => '文章标题最大长度为200',
        'new_title.min'      => '文章标题最小长度为1',
        'new_flag.max' => '文章属性最大长度为255', 		

        'new_key.max'       => '关键词最大长度为255',
    	'new_titleshort.max' => '副标题最大长度为255',
        'new_scontent.max'       => '简介最大长度为255',
        'new_zaddress.url'       => '跳转地址无效',
        'new_showtime.date'       => '显示时间无效',
    ];

    /**
    * 自定义文章属性验证规则
    * @date: 2017年4月1日 下午2:38:10
    * @author:姚荣
    * @param: $value
    * @return: $rule
    */
    protected function checkFlag($value, $rule, $data)
    {
    	$list=array('a','c','d','f','h','j','p','s');
    	if (is_array($value)){
    		foreach ($value as $k=>$v){
    			$res[$k]= in_array($v,$list);
    			if($res[$k]==false){
    				return $rule= '文章属性不符';
    			}
    		}
    	}
    	return $rule = true ;
    }
    
}
