<?php
namespace app\common\validate;

use think\Validate;

class Stores extends Validate
{
    protected $rule = [
        'store_name|店铺名' => 'require|max:30|min:1',
        'user_id|店铺归属人'   => 'require',
        'store_addr|店铺地址'      => 'require',
        'store_tel|店铺电话' => 'require|alphaDash',
    ];

    protected $message = [
        'store_name.require' => '店铺名不能为空',
        'store_name.max'     => '店铺名最大长度为30',
        'store_name.min'     => '店铺名最小长度为1',
        'user_id.require'     => '店铺归属人不能为空',
        'store_tel.require'     => '店铺归属人不能为空',
        'store_addr.require'  => '店铺地址不能为空',
        'store_tel.require'  => '店铺电话不能为空',
        'store_tel.alphaDash'      => '店铺电话包含特殊字符',
    ];
}
