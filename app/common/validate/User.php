<?php
namespace app\common\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'user_name|用户名' => 'require|max:25|min:4',
        'user_pwd|密码'   => 'require|max:25|min:6',
        'email|邮箱'      => 'email',
        'user_url|个人网站' => 'url',
        'coin|金币'       => 'number',
        'score|积分'      => 'number',
        'mobile|手机号'    => 'checkMobile:mobile',
        'user_nickname|昵称' =>'max:200|min:4',
    ];

    protected $message = [
        'user_name.require' => '用户名不能为空',
        'user_name.max'     => '用户名最大长度为25',
        'user_name.min'     => '用户名最小长度为4',
        'user_nickname.max'     => '昵称最大长度为200',
        'user_nickname.min'     => '昵称最小长度为4',
        'user_pwd.require'  => '密码不能为空',
        'user_pwd.max'      => '密码最大长度为25',
        'user_pwd.min'      => '密码最小长度为6',
    ];
    protected $scene = [
        'login' => ['user_name', 'user_pwd'],
        'edit_profile' =>['email','user_url','mobile','user_pwd'=>'max:25|min:6',],
        'edit_user' => ['user_name','email','user_pwd'=>'min:6|max:25',],
        'add_user' => ['user_name','user_pwd','email'],
    ];

    // 自定义手机验证规则
    protected function checkMobile($value, $rule, $data)
    {
        $res = inge_ismobile($value);
        return $res == true ? true : "手机号错误";
    }
}
