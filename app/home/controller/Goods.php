<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;

class Goods extends Homebase
{
    public function _initialize()
    {
        $this->three_menu[0]['text'] = '商品列表';
        $this->three_menu[0]['url']  = 'index';
        $this->three_menu[1]['text'] = '新增商品';
        $this->three_menu[1]['url']  = 'add';
        $this->assign('third_menu', $this->three_menu);
        $this->goods_category = Loader::model('GoodsCategory');
    }
    public function index()
    {
        return $this->fetch();
    }
    public function add()
    {
        $first_cat = get_goods_firstcat();
        $this->assign('first_cat',$first_cat);
        return $this->fetch('edit');
    }
    public function listorder()
    {
        $res = listorder('goods_id', input('id/a'), input('order/a'), $this->goods_category);
        if ($res) {
            return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
        }
    }
}
