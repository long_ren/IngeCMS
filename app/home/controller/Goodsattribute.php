<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;
/**
* 商品属性控制器
* @date: 2017年4月7日
* @author: 姚荣
*/
class Goodsattribute extends Homebase
{
	/**
	* 商品属性控制器构造器
	* @date: 2017年4月7日
	* @author: 姚荣
	*/
	public function _initialize()
	{
		$this->three_menu[0]['text'] = '商品属性列表';
		$this->three_menu[0]['url']  = 'index';
		$this->three_menu[1]['text'] = '新增商品属性';
		$this->three_menu[1]['url']  = 'add';
		$this->assign('third_menu', $this->three_menu);
		$this->mould = Loader::model('GoodsMould');
		$this->attr = Loader::model('GoodsAttribute');
	}
	/**
	* 商品属性列表view
	* @date: 2017年4月7日
	* @author: 姚荣
	*/
	public function index() {
		if (input('mid')){
			$list = $this->attr->alias('a')
			->join('ig_goods_mould m ','a.mould_id= m.mould_id')
			->order('a.listorder asc')->order('a.mould_id desc')
			->where('a.mould_id',input('mid'))
			->field(['a.attr_id','a.listorder','m.mould_name','attr_name','is_search','attr_way','attr_value'])
			->paginate(10);
		}else{
			$list = $this->attr->alias('a')
			->join('ig_goods_mould m ','a.mould_id= m.mould_id')
			->order('a.listorder asc')->order('a.mould_id desc')
			->field(['a.attr_id','a.listorder','m.mould_name','attr_name','is_search','attr_way','attr_value'])
			->paginate(10);
		}
		foreach ($list as $k=>$v){
			if (!empty($v['attr_value'])){
				$list[$k]['attr_value']=json_decode($v['attr_value']);
				$list[$k]['attr_value']=implode(",", $list[$k]['attr_value']);
			}
		}
		$mould=$this->mould->select();
		$this->assign('mould',$mould);
		$this->assign('list', $list);
		return $this->fetch();
	}
	/**
	 * 商品属性添加样式view
	 * @date: 2017年4月7日
	 * @author:姚荣
	 */
	public function add(){
		$mould=$this->mould->select();
		$this->assign('mould',$mould);
		return $this->fetch('edit');
	}
	/**
	 * 商品属性添加
	 * @date: 2017年4月7日
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_add(){
		if (IS_POST) {
			$res = $this->attr->saveAttr(input(''));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 商品属性修改VIEW
	 * @date: 2017年4月7日
	 * @author: 姚荣
	 */
	public function edit()
	{
		$id = input('id');
		if ($id) {
			$data     = $this->attr->where('attr_id', input('id'))->find();
			$mould=$this->mould->select();
			$this->assign('mould',$mould);
			$this->assign('data', $data);
			return $this->fetch('edit');
		}
	}
	/**
	 * 商品属性修改
	 * @date: 2017年4月7日
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_edit() {
		if (IS_POST) {
			$res = $this->attr->saveAttr(input(''), input('id'));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("修改成功")->setAsk("修改成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 改变商品属性列表排序状态
	 * @date: 2017年4月7日
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function listorder()
	{
		$res = listorder('attr_id', input('id/a'), input('order/a'), $this->attr);
		if ($res) {
			return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
		}
	}
	/**
	 * 改变商品属性是否检索状态
	 * @date: 2017年4月2日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function change_search()
	{
		$id             = input('id');
		$data['is_search'] = input('status');
		$res            = $this->attr->where('attr_id', $id)->update($data);
		if ($res) {
			if (input('status') == 0) {
				$status = "不检索";
			} else {
				$status = "检索";
			}
			return $this->ret->setCode(0)->setMsg($status)->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('修改状态失败')->toJson();
		}
	}
	/**
	 * 删除商品属性模型
	 * @date: 2017年4月7日
	 * @author: 姚荣
	 * @param: id(也会删除其子类下的模型)
	 * @return:json
	 */
	public function del()
	{
		$res = $this->attr->delFormat(input('id'));
		if ($res==true) {
			return $this->ret->setCode(0)->setMsg('删除成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('删除失败')->setReload()->toJson();
		}
	}
	
}