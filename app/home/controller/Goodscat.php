<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;

/**
 * 商品控制器
 * @date: 2017年4月1日 下午2:45:45
 * @author: 姚荣
 */
class Goodscat extends Homebase
{
    /**
     * 商品控制器构造器
     * @date: 2017年4月2日 下午5:02:29
     * @author: 姚荣
     */
    public function _initialize()
    {
        $this->three_menu[0]['text'] = '商品分类列表';
        $this->three_menu[0]['url']  = 'index';
        $this->three_menu[1]['text'] = '新增商品分类';
        $this->three_menu[1]['url']  = 'add';
        $this->assign('third_menu', $this->three_menu);
        $this->goods_category = Loader::model('GoodsCategory');
    }
    /**
     * 商品模型列表view
     * @date: 2017年4月2日 下午5:02:29
     * @author: 姚荣
     */
    public function index()
    {
        $list = $this->goods_category->order('listorder ASC')->select();
        $tree = new \thans\Common\tree($list);
        $list = $tree->formatArrayForTree('category_id', 'category_pid', 'name');
        //对二维数组排序
        $menu_pids = array();
        foreach ($list as $value) {
            $menu_pids[] = $value['category_pid'];
        }
        // array_multisort($menu_pids, SORT_ASC, $list);

        foreach ($list as $key => $value) {
            //根据对应的层级显示  模块\控制器\方法
            $list[$key]['is_show'] = $value['is_show'] == 1 ? "展示" : "不展示";
            if ($value['is_show'] == 1) {
                $list[$key]['is_show'] = '<span class="label label-success status" data-href="' . url('change_show', 'id=' . $value['category_id']) . '" data-status="1" data-id="' . $value['category_id'] . '">展示</span>';
            }else{
            	$list[$key]['is_show'] = '<span class="label label-danger status" data-href="' . url('change_show', 'id=' . $value['category_id']) . '" data-status="0" data-id="' . $value['category_id'] . '">不展示</span>';
            }
            if ($value['is_hot'] == 1) {
                $list[$key]['is_hot'] = '<span class="label label-success status" data-href="' . url('change_hot', 'id=' . $value['category_id']) . '" data-status="1" data-id="' . $value['category_id'] . '">推荐</span>';
            }else{
            	$list[$key]['is_hot'] = '<span class="label label-danger status" data-href="' . url('change_hot', 'id=' . $value['category_id']) . '" data-status="0" data-id="' . $value['category_id'] . '">不推荐</span>';
            }
            // 
            $list[$key]['action'] = '<a href="' . url('edit', 'id=' . $list[$key]['id']) . '"><i class="fa fa-pencil green control"></i></a>&nbsp;<i class="fa fa-trash-o red control confirm-rst-url-btn" data-url="' . url('del', 'id=' . $list[$key]['id']) . '" data-info="您确定要删除分类吗？<br/>PS:如果有子分类将一起被删除！" title="删除"></i>';
        }
        $tree->initArr($list);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│ ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $str = "<tr><td class='pxinput'><input type='hidden' name='id[]' value='\$id'><input type='text' name='order[]' value='\$listorder' class='pxinput'></td>
        <td>\$id</td>
        <td>\$spacer\$name</td>
        <td>\$spacer\$mobile_name</td>
        <td>\$level</td>
        <td>\$is_show</td>
        <td>\$is_hot</td>
        <td>\$action</td></tr>";
        $list = $tree->get_tree(0, $str);
        $this->assign('list', $list);
        return $this->fetch();
    }
    /**
     * 商品模型添加样式view
     * @date: 2017年4月2日 下午5:03:51
     * @author:姚荣
     */
    public function add()
    {
        $this->assign('menulist', $this->get_cate_tree(0));
        return $this->fetch('edit');
    }
    /**
     * 商品模型添加
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function go_add()
    {
        if (IS_POST) {
            $res = $this->goods_category->saveCategory(input(''));
            // var_dump($res);
            if ($res['code']) {
                return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
            }
        }
    }
    /**
     * 商品模型修改VIEW
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     */
    public function edit()
    {
        $id = input('id');
        if ($id) {
            $data = $this->goods_category->where('category_id', input('id'))->find();
            $this->assign('menulist', $this->get_cate_tree($data['category_pid']));
            $this->assign('data', $data);
            return $this->fetch('edit');
        }
    }
    /**
     * 商品模型修改
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function go_edit()
    {
        if (IS_POST) {
            $res = $this->goods_category->saveCategory(input(''), input('id'));
            if ($res['code']) {
                return $this->ret->setCode(0)->setMsg("修改成功")->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
            }
        }
    }
    /**
     * 改变商品模型显示状态
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function change_show()
    {
        $id              = input('id');
        $data['is_show'] = input('status');
        $res             = $this->goods_category->where('category_id', $id)->update($data);
        if ($res) {
            if (input('status') == 0) {
                $status = "不展示";
            } else {
                $status = "展示";
            }
            return $this->ret->setCode(0)->setMsg($status)->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('修改状态失败')->toJson();
        }
    }
    /**
     * 改变商品模型推荐状态
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function change_hot()
    {
        $id             = input('id');
        $data['is_hot'] = input('status');
        $res            = $this->goods_category->where('category_id', $id)->update($data);
        if ($res) {
            if (input('status') == 0) {
                $status = "不推荐";
            } else {
                $status = "推荐";
            }
            return $this->ret->setCode(0)->setMsg($status)->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('修改状态失败')->toJson();
        }
    }
    /**
     * 改变商品模型列表排序状态
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function listorder()
    {
        $res = listorder('category_id', input('id/a'), input('order/a'), $this->goods_category);
        if ($res) {
            return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
        }
    }
    /**
     * 改变商品模型树状单选框
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: post
     * @return:json
     */
    public function get_cate_tree($select = 0)
    {
        if (!$select) {
            $select = 0;
        }
        $list = $this->goods_category->select();
        $tree = $foo = new \thans\Common\tree($list);
        $tree->formatArrayForTree('category_id', 'category_pid', 'name');
        $data = $tree->get_tree(0, "<option value='\$id' \$selected>\$spacer \$name </option>", $select);
        return $data;
    }
    /**
     * 删除商品模型
     * @date: 2017年4月2日 下午5:04:58
     * @author: 姚荣
     * @param: id(也会删除其子类下的模型)
     * @return:json
     */
    public function del()
    {
        $res = $this->goods_category->delCate(input('id'));
        if ($res == true) {
            return $this->ret->setCode(0)->setMsg('删除成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('删除失败')->setReload()->toJson();
        }
    }
    /**
     * 获取下级分类
     * @return [type] [description]
     */
    public function get_lower_level_cat()
    {
        if(input('catid')==0){
            return $this->ret->setCode(1)->setMsg('无下级分类')->toJson();
        }
        $res = $this->goods_category->getLowerLevelCat(input('catid'));
        if($res){
            return $this->ret->setCode(0)->setMsg('获取下级分类成功')->setData($res)->toJson();
        }else{
            return $this->ret->setCode(1)->setMsg('无下级分类')->toJson();
        }
    }
}
