<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;
/**
* 商品规格控制器
* @date: 2017年4月1日 下午2:45:45
* @author: 姚荣
*/
class Goodsformat extends Homebase
{
	/**
	* 商品规格控制器构造器
	* @date: 2017年4月5日 下午5:02:29
	* @author: 姚荣
	*/
	public function _initialize()
	{
		$this->three_menu[0]['text'] = '商品规格列表';
		$this->three_menu[0]['url']  = 'index';
		$this->three_menu[1]['text'] = '新增商品规格';
		$this->three_menu[1]['url']  = 'add';
		$this->assign('third_menu', $this->three_menu);
		$this->goods_category = Loader::model('GoodsCategory');
		$this->format = Loader::model('GoodsFormat');
	}
	/**
	* 商品规格列表view
	* @date: 2017年4月5日 下午5:02:29
	* @author: 姚荣
	*/
	public function index() {
		$list = $this->format->alias('f')
							    	->join('ig_goods_category c','f.cate_id= c.category_id','LEFT')
							    	->order('f.listorder asc')->order('f.cate_id desc')
							    	->field(['f.cate_id','f.listorder','c.name','format_id','format_name','format_desc',])
							    	->paginate(10);
		$this->assign('list', $list);
		return $this->fetch();
	}
	/**
	 * 商品规格添加样式view
	 * @date: 2017年4月5日 下午5:03:51
	 * @author:姚荣
	 */
	public function add(){
		$this->assign('menulist', $this->get_cate_tree(0));
		$category=$this->goods_category->select();
		$this->assign('category',$category);
		return $this->fetch('edit');
	}
	/**
	 * 商品规格添加
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_add(){
		if (IS_POST) {
			$res = $this->format->saveFormat(input(''));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 商品规格修改VIEW
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 */
	public function edit()
	{
		$id = input('id');
		if ($id) {
			$data     = $this->format->where('format_id', input('id'))->find();
			$category=$this->goods_category->select();
			$this->assign('category',$category);
			$this->assign('menulist', $this->get_cate_tree($data['cate_id']));
			$this->assign('data', $data);
			return $this->fetch('edit');
		}
	}
	/**
	 * 商品规格修改
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_edit() {
		if (IS_POST) {
			$res = $this->format->saveFormat(input(''), input('id'));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("修改成功")->setAsk("修改成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 改变商品规格列表排序状态
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function listorder()
	{
		$res = listorder('format_id', input('id/a'), input('order/a'), $this->format);
		if ($res) {
			return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
		}
	}
	/**
	 * 查询商品分类树状单选框
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function get_cate_tree($select = 0)
	{
		if (!$select) {
			$select = 0;
		}
		$list = $this->goods_category->select();
		$tree = $foo = new \thans\Common\tree($list);
		$tree->formatArrayForTree('category_id', 'category_pid', 'name');
		$data = $tree->get_tree(0, "<option value='\$id' \$selected>\$spacer \$name </option>", $select);
		return $data;
	}
	/**
	 * 删除商品规格模型
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: id(也会删除其子类下的模型)
	 * @return:json
	 */
	public function del()
	{
		$res = $this->format->delFormat(input('id'));
		if ($res==true) {
			return $this->ret->setCode(0)->setMsg('删除成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('删除失败')->setReload()->toJson();
		}
	}
	
	
	
}