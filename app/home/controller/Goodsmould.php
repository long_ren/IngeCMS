<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;;
/**
* 商品模型控制器
* @date: 2017年4月1日 下午2:45:45
* @author: 姚荣
*/
class Goodsmould extends Homebase
{
	/**
	* 商品模型控制器构造器
	* @date: 2017年4月6日 下午5:02:29
	* @author: 姚荣
	*/
	public function _initialize()
	{
		$this->three_menu[0]['text'] = '商品模型列表';
		$this->three_menu[0]['url']  = 'index';
		$this->three_menu[1]['text'] = '新增商品模型';
		$this->three_menu[1]['url']  = 'add';
		$this->assign('third_menu', $this->three_menu);
		$this->goods_category = Loader::model('GoodsCategory');
		$this->brand = Loader::model('GoodsBrand');
		$this->format=Loader::model('GoodsFormat');
		$this->mould=loader::model('GoodsMould');
	}
	/**
	* 商品模型列表view
	* @date: 2017年4月6日 下午5:02:29
	* @author: 姚荣
	*/
	public function index()
	{
		$list = $this->mould->alias('m')
						->join('ig_goods_category c','m.cate_id= c.category_id','LEFT')
						->order('m.listorder asc')->order('m.cate_id desc')
						->field(['m.cate_id','m.listorder','c.name','mould_id','mould_name','mould_desc',])
						->paginate(10);
			
		$this->assign('list', $list);
		return $this->fetch();
	}
	/**
	 * 商品模型添加样式view
	 * @date: 2017年4月6日 下午5:03:51
	 * @author:姚荣
	 */
	public function add()
	{
		$this->assign('menulist', $this->get_cate_tree(0));
		$brand_str=$this->set_brand();
		$format_str=$this->set_format();
		$this->assign('brand', $brand_str);
		$this->assign('format', $format_str);
		return $this->fetch('edit');
	}
	/**
	 * 商品模型添加
	 * @date: 2017年4月6日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_add()
	{
		if (IS_POST) {
			$res = $this->mould->saveMould(input(''));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 商品模型修改VIEW
	 * @date: 2017年4月6日 下午5:04:58
	 * @author: 姚荣
	 */
	public function edit()
	{
		$id = input('id');
		if ($id) {
			$brand_str=$this->set_brand($id);
			$format_str=$this->set_format($id);
			$data     = $this->mould->where('mould_id', input('id'))->find();
			
			$this->assign('brand', $brand_str);
			$this->assign('format', $format_str);
			$this->assign('menulist', $this->get_cate_tree($data['cate_id']));
			$this->assign('data', $data);
			return $this->fetch('edit');
		}
	}
	/**
	 * 商品模型修改
	 * @date: 2017年4月6日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function go_edit()
	{
		if (IS_POST) {
			$res = $this->mould->saveMould(input(''), input('id'));
			if ($res['code']) {
				return $this->ret->setCode(0)->setMsg("修改成功")->setAsk("修改成功,是否继续？")->toJson();
			} else {
				return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
			}
		}
	}
	/**
	 * 改变商品模型列表排序状态
	 * @date: 2017年4月6日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function listorder()
	{
		$res = listorder('mould_id', input('id/a'), input('order/a'), $this->mould);
		if ($res) {
			return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
		}
	}
	/**
	 * 查询商品分类树状单选框
	 * @date: 2017年4月5日 下午5:04:58
	 * @author: 姚荣
	 * @param: post
	 * @return:json
	 */
	public function get_cate_tree($select = 0)
	{
		if (!$select) {
			$select = 0;
		}
		$list = $this->goods_category->select();
		$tree = $foo = new \thans\Common\tree($list);
		$tree->formatArrayForTree('category_id', 'category_pid', 'name');
		$data = $tree->get_tree(0, "<option value='\$id' \$selected>\$spacer \$name </option>", $select);
		return $data;
	}

	public function del()
	{
		$res = $this->mould->delMould(input('id'));
		if ($res==true) {
			return $this->ret->setCode(0)->setMsg('删除成功')->setReload()->toJson();
		} else {
			return $this->ret->setCode(1)->setMsg('删除失败')->setReload()->toJson();
		}
	}
	
	/**
	* 遍历得到品牌列表并按分类排序
	* @date: 2017年4月10日 下午3:35:26
	 * @author: 姚荣r
	 * @return:string
	*/
	private function set_brand($id='')
	{
		$category=$this->goods_category->field('category_id,name')->select();
		$str='<div class="col-sm-12"><span class="col-sm-2">默认</span>';
		//传入模型ID时候的品牌选定
		if($id){
			$data     = $this->mould->where('mould_id', input('id'))->column('mould_brand');
			$mould_brand=json_decode($data['0'],true);
			$brand=$this->brand->where('cate_id',0) ->select();
			//查找默认品牌
			if (!empty($brand)){
				foreach ($brand  as $v){
					if (is_array($mould_brand)&&in_array($v['brand_id'], $mould_brand)){
						$check='checked';
					}else{
						$check='';
					}
					$str.='<input type="checkbox" name="mould_brand[]" class="flat-blue" value="'.$v['brand_id'].'"'.$check.'>'.$v['brand_name'].'&nbsp;&nbsp;';
				}
			}
			//根据分类查找品牌
			foreach($category as $k=>$v){
				$brand=$this->brand->where('cate_id',$v['category_id']) ->select();
				if (!empty($brand)){
					$str.='</div><div class="col-sm-12"><span class="col-sm-2 ">'.$v['name'].'</span>';
					foreach ($brand  as $v){
						if (is_array($mould_brand)&&in_array($v['brand_id'], $mould_brand)){
							$checked='checked';
						}else{
							$checked='';
						}
						$str.='<input type="checkbox" name="mould_brand[]" class="flat-blue" value="'.$v['brand_id'].'"'.$checked.'>'.$v['brand_name'].'&nbsp;&nbsp;';
					}
				}
			}
			$str.='</div>';
		}else
		//默认品牌选定
		{
			$brand=$this->brand->where('cate_id',0) ->select();
			//查找默认品牌
			if (!empty($brand)){
				foreach ($brand  as $v){
					$str.='<input type="checkbox" name="mould_brand[]" class="flat-blue" value="'.$v['brand_id'].'">'.$v['brand_name'].'&nbsp;&nbsp;';
				}
			}
			//根据分类查找品牌
			foreach($category as $k=>$v){
				$brand=$this->brand->where('cate_id',$v['category_id']) ->select();
				if (!empty($brand)){
					$str.='</div><div class="col-sm-12"><span class="col-sm-2 ">'.$v['name'].'</span>';
					foreach ($brand  as $v){
						$str.='<input type="checkbox" name="mould_brand[]" class="flat-blue" value="'.$v['brand_id'].'">'.$v['brand_name'].'&nbsp;&nbsp;';
					}
				}
			}
			
			$str.='</div>';
		}

		return $str;
	}
	
	/**
	 * 遍历得到规格列表并按分类排序
	 * @date: 2017年4月10日 下午3:35:26
	 * @author: 姚荣r
	 * @return:string
	 */
	private function set_format($id='')
	{
		$category=$this->goods_category->field('category_id,name')->select();
		//查找默认规格
		$str='<div class="col-sm-12"><span class="col-sm-2">默认</span>';
		//传入模型ID时候的规格选定
		if($id){
			$data     = $this->mould->where('mould_id', input('id'))->column('mould_format');
			$mould_format=json_decode($data['0'],true);
			$format=$this->format->where('cate_id',0) ->select();
			//查找默认品牌
			if (!empty($format)){
				foreach ($format  as $v){
					if (is_array($mould_format)&&in_array($v['format_id'], $mould_format)){
						$check='checked';
					}else{
						$check='';
					}
					$str.='<input type="checkbox" name="mould_format[]" class="flat-blue" value="'.$v['format_id'].'"'.$check.'>'.$v['format_name'].'&nbsp;&nbsp;';
				}
			}
			//根据分类查找品牌
			foreach($category as $k=>$v){
				$format=$this->format->where('cate_id',$v['category_id']) ->select();
				if (!empty($format)){
					$str.='</div><div class="col-sm-12"><span class="col-sm-2 ">'.$v['name'].'</span>';
					foreach ($format  as $v){
						if (is_array($mould_format)&&in_array($v['format_id'], $mould_format)){
							$checked='checked';
						}else{
							$checked='';
						}
						$str.='<input type="checkbox" name="mould_format[]" class="flat-blue" value="'.$v['format_id'].'"'.$checked.'>'.$v['format_name'].'&nbsp;&nbsp;';
					}
				}
			}
			$str.='</div>';
		}else
		//默认规格选定
		{
			$format=$this->format->where('cate_id',0) ->select();
			if (!empty($format)){
				foreach ($format  as $v){
					$str.='<input type="checkbox" name="mould_format[]" class="flat-blue" value="'.$v['format_id'].'">'.$v['format_name'].'&nbsp;&nbsp;';
				}
			}
			//根据分类查找规格
			foreach($category as $k=>$v){
				$format=$this->format->where('cate_id',$v['category_id']) ->select();
				if (!empty($format)){
					$str.='</div><div class="col-sm-12"><span class="col-sm-2 ">'.$v['name'].'</span>';
					foreach ($format  as $v){
						$str.='<input type="checkbox" name="mould_format[]" class="flat-blue" value="'.$v['format_id'].'">'.$v['format_name'].'&nbsp;&nbsp;';
					}
				}
			}
			$str.='</div>';
		}
		
		
		
		
		
		
		
		return $str;
	}
	
}