<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;

class Setting extends Homebase
{
    public function _initialize()
    {
        /**
         * 基本设置三级导航
         */
        $this->menu_base[0]['text'] = '基本信息';
        $this->menu_base[0]['url']  = 'site';
        $this->menu_base[1]['text'] = 'SEO信息';
        $this->menu_base[1]['url']  = 'seo';
        $this->menu_base[2]['text'] = 'OSS信息';
        $this->menu_base[2]['url']  = 'oss';
        $this->options              = Loader::model('Options');
        // $this->menu_base[2]['text'] = '短信API配置';
        // $this->menu_base[2]['url']  = 'smsapi';
        // $this->menu_base[3]['text'] = '微信配置';
        // $this->menu_base[3]['url']  = 'wxset';
    }
    /**
     * 站点基本设置
     * @return [type] [description]
     */
    public function site()
    {
        $this->assign('third_menu', $this->menu_base);
        return $this->fetch();
    }
    
    /**
    *修改站点基本设置并绑定OSS服务
    * @date: 2017年3月31日 下午1:40:04
    * @author: 姚荣
    * @param: post
    * @return:json
    */
    function go_site(){
    	if (IS_POST) {
    		$options=input('')['options'];
    		$res = $this->options->saveOptions('site_options',$options,1);
    		if($res){
    			return $this->ret->setCode(0)->setMsg('配置更新成功')->toJson();
    		}else{
    			return $this->ret->setCode(1)->setMsg($res)->toJson();
    		}
    	}
    }
    
    public function seo()
    {
        $this->assign('third_menu', $this->menu_base);
        return $this->fetch();
    }
    
   /**
   * 修改SEO信息
   * @date: 2017年3月31日 下午1:40:50
   * @author: 姚荣
   * @param: post
   * @return:json
   */
    public function go_seo(){
    	if (IS_POST) {
    		$res = $this->options->saveOptions('seo_options',input('')['options'],1);
    		if($res){
    			return $this->ret->setCode(0)->setMsg('配置更新成功')->toJson();
    		}else{
    			return $this->ret->setCode(1)->setMsg($res)->toJson();
    		}
    	}
    }
    
    public function wxset()
    {
        $this->assign('third_menu', $this->menu_base);
        return $this->fetch();
    }
    
    
    public function smsapi()
    {
        $this->assign('third_menu', $this->menu_base);
        return $this->fetch();
    }
    /**
    * OSS信息修改页面
    * @date: 2017年4月1日 下午4:55:10
    * @author: 姚荣
    * @param: variable
    * @return:
    */
    public function oss(){
    	$oss_value=get_oss_options();
    	$this->assign('oss',$oss_value['oss']);
    	$this->assign('oss_keyid',$oss_value['oss_keyid']);
    	$this->assign('oss_ackey',$oss_value['oss_ackey']);
    	$this->assign('oss_end_point',$oss_value['oss_end_point']);
    	$this->assign('oss_iscname',$oss_value['oss_iscname']);
    	$this->assign('oss_bucket',$oss_value['oss_bucket']);
    	$this->assign('oss_basepath',$oss_value['oss_basepath']);
    	$this->assign('third_menu', $this->menu_base);
    	return $this->fetch();
    }
    
    /**
     * OSS添加
     * @date: 2017年4月1日 下午4:55:10
     * @author: 姚荣
     * @param: variable
     * @return:
     */
    public function go_oss(){
    	if (IS_POST) {
    		$options=input('')['options'];
	    	if (!isset($options['oss'])){
	    		$options['oss']='0';
	    	}
	    	if (isset($options['oss_iscname'])){
	    		$options['oss_iscname']=true;
	    	}else {
	    		$options['oss_iscname']=false;
	    	}
	    	$res = $this->options->saveOptions('oss_options',$options,0);
	    	if($res){
	    		return $this->ret->setCode(0)->setMsg('配置更新成功')->setReload()->toJson();
	    	}else{
	    		return $this->ret->setCode(1)->setMsg($res)->toJson();
	    	}
    	}
    }
    

}
