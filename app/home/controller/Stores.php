<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \thans\Ret;
use \think\Loader;

class Stores extends Homebase
{

	    public function _initialize()
	    {
	        $this->store    = Loader::model('Stores');
	        $this->user    = Loader::model('user');
	        $this->datenow =date("y-m-d h:i:s" ,time());
	        /**
	         * 门店三级菜单
	         */
	        $this->smenu[0]['text'] = '门店列表';
	        $this->smenu[0]['url']  = 'index';
	        $this->smenu[1]['text'] = '新增门店';
	        $this->smenu[1]['url']  = 'add';
	
	    }
	    
	    public function index()
	    {
	    	$this->assign('third_menu', $this->smenu);
	    	$store_list = $this->store->alias('s')
							    	->join('ig_user u','s.user_id= u.user_id')
 							    	->field(['s.store_id','s.status','store_name','s.user_id','user_name','store_desc','store_tel','store_addr','store_nameshort','update_ip','s.update_date',])
							    	->where('s.is_del',0)
							    	->paginate(10);

	    	$this->assign('storelist', $store_list);
	    	return $this->fetch();
	    }
	    
	    
	    public function add()
	    {
	    		$this->assign('third_menu', $this->smenu);
	    		$admin= $this->user->where('is_del', 0)->where('user_type', 1)->select();
	    		$this->assign('admin',$admin);
	    		return $this->fetch('edit');
	    }
	    
	    public function edit()
	    {
	    	$sid = input('sid');
	    	if ($sid) {
	    		$store_res = $this->store->getStoreById($sid);
	    		$admin= $this->user->where('is_del', 0)->where('user_type', 1)->select();
	    		$this->assign('admin',$admin);
	    		$this->assign('data', $store_res);
	    		$this->assign('third_menu', $this->smenu);
	    		return $this->fetch();
	    	}
	    }
	    
	    public function go_add()
	    {
	    	if (IS_POST) {
	    		$data = $this->request->only(['store_name','user_id','store_addr','store_tel','store_desc','store_nameshort'],'post');	    		
				$data['creater_date']=$this->datenow;
				$data['update_date']=$this->datenow;		
				$data['creater_user_id']= session('u_id');
				$data['creater_ip']=inge_get_ip();
				$data['update_ip']=inge_get_ip();
				if (isset($data['status'])) {
					$data['status'] = '1';
				} else {
					$data['status'] = '0';
				}
				//保存数据
				$result = $this->store->checkForm($data);
				if(is_string($result)){
					return $this->ret->setCode(1)->setMsg($result)->toJson();
				}
	    		$res = $this->store->saveStore($data);
	    		if ($res['code']) {
	    			return $this->ret->setCode(0)->setAsk("新增成功,是否继续？")->toJson();
	    		} else {
	    			return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
	    		}
	    	}
	    }
	    
	    public function go_edit()
	    {
	    	$sid = input('sid');
	    	if (IS_POST) {
	    		$data = input('post.');
	    		//缺少字段维护update_ip，update_date，******，creater_date，creater_user_id，creater_ip
	    		$data['update_date']=$this->datenow;
	    		$data['update_ip']=inge_get_ip();
	    		if (isset($data['status'])) {
	    			$data['status'] = '1';
	    		} else {
	    			$data['status'] = '0';
	    		}
	    		$result = $this->store->checkForm($data);
				if(is_string($result)){
					return $this->ret->setCode(1)->setMsg($result)->toJson();
				}
	    		//保存数据
	    		$res = $this->store->saveStore($data, $sid);
	    		if ($res['code']) {
	    			return $this->ret->setCode(0)->setMsg($res['msg'])->setBack()->toJson();
	    		} else {
	    			return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
	    		}
	    	}
	    }
	  
	    
	    public function change_status()
	    {
	    	$sid = input('sid');
	    	$data['status'] = input('status');
	    	$res   = $this->store->where('store_id', $sid)->update($data);
	    	if ($res) {
	    		if ($data['status'] == 1) {
	    			$status = "启用";
	    		} else {
	    			$status = "停用";
	    		}
	    		return $this->ret->setCode(0)->setMsg($status)->toJson();
	    	} else {
	    		return $this->ret->setCode(1)->setMsg('修改状态失败')->toJson();
	    	}
	    }
}