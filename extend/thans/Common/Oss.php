<?php
namespace thans\Common;

use OSS\OssClient;
use OSS\Core\OssException;
/**
* 实例化OSS对象，封装：上传远端，远端删除，路径获取
* @date: 2017年3月30日 下午6:47:05
* @author: 姚荣
*/
class Oss{
	private $key_id; // 从OSS获得的AccessKeyId
	private $key_secret ; // 从OSS获得的AccessKeySecret
	private $end_point ; // 您选定的OSS数据中心访问域名，例如oss-cn-hangzhou.aliyuncs.com
	private $bucket; // 存储空间
	protected $oss; // 存储OSS对象
	
	/**
	 *  实例化OSS对象，默认传绑定好的域名
	 * @date: 2017年3月30日 下午6:40:15
	 * @author: 姚荣
	 * @param: $keyId,$keySecret，$endPoint，$bucket，$isCName
	 * @return:ObjectInfo
	 */
	public function init($keyId,$keySecret,$endPoint,$bucket,$isCName = true)
	{
		$this->key_id = $keyId;
		$this->key_secret = $keySecret;
		$this->end_point = $endPoint;
		$this->bucket=$bucket;
		try {
			$this->oss = new OssClient ( $this->key_id, $this->key_secret, $this->end_point,$isCName);
		}catch (OssException $e) {
			$msg['code']='0';
			$msg['msg']='错误的创建';
			$msg['error_log']= $e->getMessage();
			return $msg;
        }
		return $this->oss;
	}
	
	/**
	*  OSS文件上传
	* @date: 2017年3月30日 下午6:40:15
	* @author: 姚荣
	* @param: $file_path(文件路径),$oss_path(上传目录)
	* @return:$msg['code','msg','data(远端路径$oss_path)']
	*/
	public function oss_upload($file_path,$oss_path)
	{
			
			if (file_exists ( $file_path)) {
				try {
					$this->oss->uploadFile ($this->bucket, $oss_path, $file_path ); // 上传到OSS
				}catch (OssException $e) {
					$msg['code']='0';
					$msg['msg']='上传失败';
					$msg['error_log']= $e->getMessage();
					return $msg;
				}
				// 如需上传到oss后 自动删除本地的文件 则删除下面的注释
				unlink($file_path);
				$msg['code']='1';
				$msg['msg']='上传成功';
				$msg['data']=$this->get_url($oss_path);
				return $msg;
			}
				$msg['code']='0';
				$msg['msg']='不是正确的路径';
				$msg['error_log']='文件上传路径:'.$oss_path.'||服务器文件路径:'.$file_path;
				return $msg;
	}
	
	/**
	 *  获取上传对象URL
	 * @date: 2017年3月30日 下午6:40:15
	 * @author: 姚荣
	 * @param: $path(远端文件路径)
	 * @return:$path(完整路径)
	 */
	public function get_url($path)
	{
			if (empty ( $path )) {
				 return false;
			}
			
			if (strpos ( $path, 'http://' ) !== false) {//  如果已经有http直接返回
				return $path;
			}
			return 'http://' .$this->end_point. '/'.$path;
	}
	
	/**
	 * 删除上传对象
	 * @date: 2017年3月30日 下午6:40:15
	 * @author: 姚荣
	 * @param: $object(远端文件对象)
	 * @return:$msg
	 */
	public function oss_delet_object($object) {
		try {
			$res = $this->oss->deleteObject ( $this->bucket, $object );
		}catch (OssException $e) {
			$msg['code']='0';
			$msg['msg']='删除失败';
			$msg['error_log']=$res;
			return $msg;
		}

		$msg['code']='1';
		$msg['msg']='删除成功';
		$msg['data']=$res;
		return $msg;
	}
}