<?php
namespace thans\Common;

class Posts
{
    public $list              = array();
    public $is_load_post_data = false;
    public $postData          = array();
    public $viewType          = 'pc'; //预览方式【pc，mobile】
    public $pluginList        = array();
    public $plugScript        = '';
    public $verfiyAfterData   = array(); //验证数据后存放
    /**
     * @param $list
     * @name   表单初始化
     */
    public function init($list)
    {
        $this->list = $list;
    }

    public function loadPostData($postData)
    {
        $this->postData          = $postData;
        $this->is_load_post_data = true;
    }

    public function getScriptAndStyle()
    {
        $html = '';

        if (in_array('umeditor', $this->pluginList) || in_array('file', $this->pluginList) || in_array('image', $this->pluginList)) {
            $html .= '<link href="/static/plugins/umeditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
            <script type="text/javascript" charset="utf-8" src="/static/plugins/umeditor/umeditor.config.js"></script>
            <script type="text/javascript" charset="utf-8" src="/static/plugins/umeditor/umeditor.min.js"></script>
            <script type="text/javascript" src="/static/plugins/umeditor/lang/zh-cn/zh-cn.js"></script>
            <script type="text/javascript">
                $(\'script[type="umeditor"]\').each(function(){
                    var name = $(this).attr(\'name\');
                    var id = $(this).attr(\'id\');
                    var um = UM.getEditor(id);
                    $(\'form.ingeAjax\').submit(function(){
                        $(\'form.ingeAjax\').find(\'textarea[name="\'+name+\'"]\').val(um.getContent());
                    });
                });
            </script>';
        }
        if (in_array('ueditor', $this->pluginList)) {
            $html .= '<script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.config.js"></script>
            <script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.all.min.js"></script>
            <script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/lang/zh-cn/zh-cn.js"></script>
            <script type="text/javascript">
                $(\'script[type="ueditor"]\').each(function(){
                    var name = $(this).attr(\'name\');
                    var id = $(this).attr(\'id\');
                    var um = UE.getEditor(id);
                    $(\'form.ingeAjax\').submit(function(){
                        $(\'form.ingeAjax\').find(\'textarea[name="\'+name+\'"]\').val(um.getContent());
                    });
                });
            </script>';
        }
        if (in_array('file', $this->pluginList) || in_array('image', $this->pluginList)) {
            $html .= "<script type=\"text/javascript\">$(function(){" . $this->plugScript . "});</script>";
            if (!in_array('ueditor', $this->pluginList)) {
                $html .= '<script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.config.js"></script>
                 <script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.all.min.js"></script>';
            }
        }
        return $html;
    }

    /**
     * @name 管理员后台预览
     */
    public function getView()
    {
        $html = '';
        foreach ($this->list as $data) {
            if ($this->viewType == 'pc') {
                $method_name = 'input' . ucfirst($data['input_way']);
                if (method_exists($this, $method_name)) {
                    $this->pluginList[] = $data['input_way'];
                    $str                = "<div class=\"form-group\">
                        <label class=\"col-sm-2 control-label no-padding-right\" for=\"form-field-1\"> {$data['module_name']}： </label>
                        <div class=\"col-sm-10\">";
                    if ($this->is_load_post_data) {
                        if (isset($this->postData[$data['module_id']])) {
                            $data['default_data'] = $this->postData[$data['module_id']];
                        }
                    }
                    $str .= call_user_func_array(array($this, $method_name), array(false, $data));
                    if ($data['is_request'] == 1) {
                        $str .= "<span class=\"lbl col-xs-12 col-sm-4\">&nbsp;&nbsp;<span class=\"red\">*</span>必填</span>";
                    }
                    $str .= "</div></div>";
                    $html .= $str;
                }
            }
        }
        return $html;
    }

    public function verfiy($formData)
    {
        $this->verfiyAfterData = [];
        $sqlData               = array();
        foreach ($this->list as $data) {
            // var_dump($data);
            $module_id = isset($data['module_id']) ? $data['module_id'] : '';
            // var_dump($module_id);
            if (isset($formData[$module_id])) {
                $method_name = 'input' . ucfirst($data['input_way']);
                $str         = call_user_func_array(array($this, $method_name), array(true, $data, $formData[$module_id]));
                // var_dump($data);
                // die();
                if (is_object($str)) {
                    return $str->msg;
                }
                $this->verfiyAfterData[$module_id] = $str;
            } else {
//                return '数据id不存在';
            }
        }
        return true;
    }

    /**
     * @name 获取验证后的数据
     */
    public function getVerfiyAfterData()
    {
        return $this->verfiyAfterData;
    }
    private function dataCheck($data, $formData)
    {
        $formData = trim((string) $formData);
        //验证是否为只读
        if ($data['is_only_read']) {
            return $data['default_data'];
        }
        //是否可为空验证
        if ($formData == "" && !$data['is_request']) {

        } elseif ($formData == "" && $data['is_request']) {
            return (object) array(
                'status' => false,
                'msg'    => $data['module_name'] . '不能为空',
            );
        }
        //长度验证
        if ($data['length'] && $formData != "") {
            $length = explode('-', $data['length']);
            if (count($length) == 2) {
                if (strlen($formData) >= intval($length[0]) && strlen($formData) <= intval($length[1])) {

                } else {
                    return (object) array(
                        'status' => false,
                        'msg'    => $data['module_name'] . '长度范围:' . $data['length'] . '位',
                    );
                }
            } else {
                if (strlen($formData) == intval($length[0])) {

                } else {
                    return (object) array(
                        'status' => false,
                        'msg'    => $data['module_name'] . '长度范围:' . $data['length'] . '位',
                    );
                }
            }
        }
        //正则验证
        if ($data['check_rule'] && $formData != "") {
            if (!preg_match($data['check_rule'], $formData)) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '格式错误',
                );
            }
        }
        return $formData;
    }
    /**
     * @name 文本输入框
     */
    private function inputShort_test($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $res = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"text\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 长文本输入框
     */
    private function inputLong_test($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $res = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<textarea  name=\"{$data['module_id']}\" class=\"form-group-new  col-xs-12 col-sm-4\" {$is_request} {$is_only_read}>{$data['default_data']}</textarea>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 密码输入框
     */
    private function inputPsw_test($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $res = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"password\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name ueditor
     */
    private function inputUeditor($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $res = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $this->pluginList[] = 'ueditor';
            $html .= "<script type=\"ueditor\" id=\"uEditor_{$data['module_id']}\" name=\"{$data['module_id']}\" type=\"text/plain\" style=\"width:98%;height:360px;\">{$data['default_data']}</script>";
            $html .= "<textarea style=\"display: none;\" name=\"{$data['module_id']}\" class=\"form-group-new  col-xs-12 col-sm-4\" {$is_request} {$is_only_read}></textarea>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name umeditor
     */
    private function inputUmeditor($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $res = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $this->pluginList[] = 'umeditor';
            $html .= "<script type=\"umeditor\" id=\"umeditor_{$data['module_id']}\" name=\"{$data['module_id']}\" type=\"text/plain\" style=\"width:90%;height:360px;\">{$data['default_data']}</script>";
            $html .= "<textarea style=\"display: none;\" name=\"{$data['module_id']}\" class=\"form-group-new  col-xs-12 col-sm-4\" {$is_request} {$is_only_read}></textarea>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 详细时间输入框
     */
    private function inputChoose_time($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $data['length']     = '';
            $data['check_rule'] = '';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<div class=\"input-group date col-xs-12 col-sm-4\">
                <div class=\"input-group-addon\">
                    <i class=\"fa fa-calendar\"></i>
                </div>
                <input type=\"text\" class=\"form-control pull-right detailtime\" name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>
            </div>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 详细日期输入框
     */
    private function inputChoose_details_time($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $data['length']     = '';
            $data['check_rule'] = '';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<div class=\"input-group date col-xs-12 col-sm-4\">
                <div class=\"input-group-addon\">
                    <i class=\"fa fa-calendar\"></i>
                </div>
                <input type=\"text\" class=\"form-control pull-right onlyyearmonth\" name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>
            </div>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 时间输入框
     */
    private function inputChoose_date($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $data['length']     = '';
            $data['check_rule'] = '';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<div class=\"input-group date col-xs-12 col-sm-4\">
                <div class=\"input-group-addon\">
                    <i class=\"fa fa-calendar\"></i>
                </div>
                <input type=\"text\" class=\"form-control pull-right onlytime\" name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>
            </div>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 数字输入框
     */
    private function inputNumber($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = str_replace(",","",$formData);
            $data['check_rule'] = '/^[0-9\,\.-]*$/';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"number\" class=\"form-group-new  col-xs-12 col-sm-4\" placeholder=\"数字\" name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 整形数字输入框
     */
    private function inputInt($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = str_replace(",","",$formData);
            $data['check_rule'] = '/^-?[1-9]\d*$/';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"text\" class=\"form-group-new  col-xs-12 col-sm-4\" placeholder=\"整数\" name=\"{$data['module_id']}\" data-type=\"number\" data-decimal=\"0\" data-name=\"\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 金钱输入框
     */
    private function inputMoney($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = str_replace(",","",$formData);
            $data['check_rule'] = '/^-?\d+\.\d{0,2}$/';
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<div class=\"input-group col-xs-12 col-sm-4\">
                <input type=\"text\" style=\"text-align:right;\" class=\"form-group-new col-xs-12 col-sm-12\" name=\"{$data['module_id']}\" data-type=\"number\" data-decimal=\"2\" data-name=\"\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>
                <div class=\"input-group-addon\">
                    <i class=\"fa fa-rmb\"></i>
                </div>
            </div>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 进度
     */
    private function inputPercent($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            return intval($formData);
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<div class=\"input-group col-xs-12 col-sm-4\">
                <input type=\"text\" style=\"text-align:right;\" class=\"form-group-new col-xs-12 col-sm-12\" name=\"{$data['module_id']}\" data-type=\"number\" data-decimal=\"5\" data-name=\"\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>
                <div class=\"input-group-addon\">
                    <i class=\"fa fa-percent\"></i>
                </div>
            </div>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 邮箱
     */
    private function inputEmail($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $data['length']     = '';
            $data['check_rule'] = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
            $res                = $this->dataCheck($data, $formData);
            return $res;
        }

        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"email\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 单选
     */
    private function inputRadio($verfiy = false, $data, $formData = '')
    {

        if ($verfiy) {
            $vData    = array(1, 2, 3, 4, 5, 6, 7, 8);
            $formData = trim((string) $formData);
            if ($formData == "" && !$data['is_request']) {

            } elseif ($formData == "" && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }

            if (!in_array($formData, $vData) && $formData != '') {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '数据不存在',
                );
            }
            return $formData;
        }

        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        $datas = json_decode($data['default_data'], true);
        if ($this->viewType == 'pc') {
            $i = 0;
            foreach ($datas as $key => $value) {
                if ($i == 0) {
                    $check = 'checked';
                } else {
                    $check = '';
                }
                $html .= "<input type=\"radio\" class=\"flat-blue\"  name=\"{$data['module_id']}\" {$check} value=\"{$value}\">{$key}";
                $i++;
            }
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }
    /**
     * @name 多选
     */
    private function inputCheckbox($verfiy = false, $data, $formDatas = '')
    {
        if ($verfiy) {
            $checkData = array();
            $vData     = array(1, 2, 3, 4, 5, 6, 7, 8);
            $formDatas = is_array($formDatas) ?: array();
            if (count($formDatas) == 0 && !$data['is_request']) {

            } elseif (count($formDatas) == 0 && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }
            if (count($formDatas) > 0) {
                foreach ($formDatas as $formData) {
                    if (!in_array($formData, $vData)) {
                        return (object) array(
                            'status' => false,
                            'msg'    => $data['module_name'] . '数据不存在',
                        );
                    }
                    $checkData[] = $formData;
                }
            }
            return $checkData;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        $datas = json_decode($data['default_data'], true);
        if ($this->viewType == 'pc') {
            $i = 0;
            foreach ($datas as $key => $value) {
                if ($i == 0) {
                    $check = 'checked';
                } else {
                    $check = '';
                }
                $html .= "<input type=\"checkbox\" class=\"flat-blue\"  name=\"{$data['module_id']}\" {$check} value=\"{$value}\">{$key}";
                $i++;
            }
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }
    /**
     * @name 下拉选择
     */
    private function inputSelect($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $vData    = array(1, 2, 3, 4, 5, 6, 7, 8);
            $formData = trim((string) $formData);
            if (!$formData && !$data['is_request']) {

            } elseif (!$formData && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }
            if (!in_array($formData, $vData) && $formData) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '数据不存在',
                );
            }
            return $formData;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }

        $datas = json_decode($data['default_data'], true);
        $select_title = isset($datas['title']) ? $datas['title']: '';
        unset($datas['title']);
        // var_dump($datas);
        $html .= "<select name=\"{$data['module_id']}\" class=\"select2 form-group-new  col-xs-12 col-sm-4\">";
        if($select_title){
            $html.="<option value=\"-1\" checked >{$select_title}</option>";
        }
        if ($this->viewType == 'pc') {
            $i = 0;
            $counts =count($datas);
            foreach ($datas as $key => $value) {
                // var_dump($key);
                if($counts!=1){
                    $html .= "<optgroup label=\"{$key}\">";
                }
                foreach ($value as $k => $v) {
                    if ($i == 0 && !$select_title) {
                        $check = 'checked';
                    } else {
                        $check = '';
                    }
                    $html .= "<option value=\"{$v}\" {$check} >{$k}</option>";
                    $i++;
                }
                if($counts!=1){
                    $html .= '</optgroup>';
                }
            }
            $html .= "</select>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 下拉多选
     */
    private function inputSelect_mul($verfiy = false, $data, $formDatas = '')
    {
        if ($verfiy) {
            $checkData = array();
            $vData     = array(1, 2, 3, 4, 5, 6, 7, 8);
            $formDatas = is_array($formDatas) ?: array();
            if (!$formData && !$data['is_request']) {

            } elseif (!$formData && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }
            foreach ($formDatas as $formData) {
                if (!in_array($formData, $vData)) {
                    return (object) array(
                        'status' => false,
                        'msg'    => $data['module_name'] . '数据不存在',
                    );
                }
                $checkData[] = $formData;
            }
            return $checkData;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }

        $datas = json_decode($data['default_data'], true);
        $select_title = isset($datas['title']) ? $datas['title']: '';
        unset($datas['title']);
        // var_dump($datas);
        $html .= "<select name=\"{$data['module_id']}\" data-max='{$data['length']}' multiple=\"multiple\" class=\"select2_ form-group-new  col-xs-12 col-sm-4\">";
        if($select_title){
            $html.="<option value=\"-1\" checked >{$select_title}</option>";
        }
        if ($this->viewType == 'pc') {
            $i = 0;
            $counts =count($datas);
            foreach ($datas as $key => $value) {
                // var_dump($key);
                if($counts!=1){
                    $html .= "<optgroup label=\"{$key}\">";
                }
                foreach ($value as $k => $v) {
                    if ($i == 0 && !$select_title) {
                        $check = 'checked';
                    } else {
                        $check = '';
                    }
                    $html .= "<option value=\"{$v}\" {$check} >{$k}</option>";
                    $i++;
                }
                if($counts!=1){
                    $html .= '</optgroup>';
                }
            }
            $html .= "</select>";
        } else if ($this->viewType == 'mobile') {

        }

        return $html;
    }

    /**
     * @name 填表人
     */
    private function inputPeople($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            // var_dump($_SESSION['think']['u_id']);die();
            return $_SESSION['think']['u_id'];
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"people\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 文件上传
     */
    private function inputFile($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = trim((string) $formData);
            if (!$formData && !$data['is_request']) {

            } elseif (!$formData && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }
            return $formData;
        }
        $html               = '';
        $this->pluginList[] = 'file';
        $is_request         = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<script type=\"text/plain\" id=\"upload_ue_{$data['module_id']}\"></script><input type=\"text\" class=\"form-group-new  col-xs-12 col-sm-4\" name=\"{$data['module_id']}\"  readonly=\"true\" value=\"{$data['default_data']}\"  {$is_request} {$is_only_read}／>
            <a name=\"{$data['module_id']}\" href=\"javascript:;\" class=\"btn btn-info\" style=\"margin-left: 10px;\">上传文件</a>";
            $this->plugScript .= "
                var upload_ue_{$data['module_id']} = UE.getEditor('upload_ue_{$data['module_id']}',{
                    initialFrameWidth:0,
                    initialFrameHeight:0,
                });
                upload_ue_{$data['module_id']}.ready(function () {
                    upload_ue_{$data['module_id']}.setDisabled();
                    upload_ue_{$data['module_id']}.hide();
                    upload_ue_{$data['module_id']}.addListener('afterUpfile', function (t, arg) {
                        $('input[name=\"{$data['module_id']}\"').val(arg[0].url);
                    });
                });
                $('a[name=\"{$data['module_id']}\"]').on('click',function(){
                    var myFiles = upload_ue_{$data['module_id']}.getDialog('attachment');
                    myFiles.open();
                });
            ";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 图片上传
     */
    private function inputImage_file($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = trim((string) $formData);
            if (!$formData && !$data['is_request']) {

            } elseif (!$formData && $data['is_request']) {
                return (object) array(
                    'status' => false,
                    'msg'    => $data['module_name'] . '不能为空',
                );
            }
            return $formData;
        }
        $html               = '';
        $this->pluginList[] = 'image';
        $is_request         = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            if(!$data['default_data']){
                $data['default_data'] = '/static/img/no_img.jpg';
            }
            $html .= "<script type=\"text/plain\" id=\"upload_ue_{$data['module_id']}\"></script><input type=\"hidden\" class=\"form-group-new  col-xs-12 col-sm-4\" name=\"{$data['module_id']}\" value=\"{$data['default_data']}\"  {$is_request} {$is_only_read}／><img  class=\"{$data['module_id']}\" src=\"{$data['default_data']}\" >
               <a name=\"{$data['module_id']}\" href=\"javascript:;\" class=\"btn btn-info\" style=\"margin-left: 10px;\">上传图片</a>";
            $this->plugScript .= "
                var upload_ue_{$data['module_id']} = UE.getEditor('upload_ue_{$data['module_id']}',{
                    initialFrameWidth:0,
                        initialFrameHeight:0,
                });
                upload_ue_{$data['module_id']}.ready(function () {
                    upload_ue_{$data['module_id']}.setDisabled();
                    upload_ue_{$data['module_id']}.hide();
                    upload_ue_{$data['module_id']}.addListener('beforeInsertImage', function (t, arg) {
                    console.log(arg);
                        $('input[name=\"{$data['module_id']}\"').val(arg[0].src);
                        $('.{$data['module_id']}').attr('src',arg[0].src);
                        $('.{$data['module_id']}').attr('width','200px');
                        $('.{$data['module_id']}').attr('height','auto');
                    });
                });
                $('a[name=\"{$data['module_id']}\"]').on('click',function(){
                    var myImage = upload_ue_{$data['module_id']}.getDialog('insertimage');
                    myImage.open();
                });
            ";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 随机字符串
     */
    private function inputRandom_data($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = trim((string) $formData);
            return $formData;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $random = inge_get_random_str(32);
            $html .= "<input type=\"hidden\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"$random\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

    /**
     * @name 系统数据
     */
    private function inputSys_data($verfiy = false, $data, $formData = '')
    {
        if ($verfiy) {
            $formData = trim((string) $formData);
            return $formData;
        }
        $html       = '';
        $is_request = '';
        if ($data['is_request']) {
            $is_request = 'required="required"';
        }
        $is_only_read = '';
        if ($data['is_only_read']) {
            $is_only_read = 'readOnly="true"';
        }
        if ($this->viewType == 'pc') {
            $html .= "<input type=\"sys_data\" class=\"form-group-new  col-xs-12 col-sm-4\"  name=\"{$data['module_id']}\" value=\"{$data['default_data']}\" {$is_request} {$is_only_read}>";
        } else if ($this->viewType == 'mobile') {

        }
        return $html;
    }

}
