<?php
namespace thans\Common;
use thans\Common\Http;

class Sms{
	
	public $username='sanshi';
	public $password='qp6tsjib';
	public $mobile;
	public $content;
	public $dstime;
	public $ext;
	public $msgid;
	/**
	 * 发送HTTP请求
	 *
	 * @param string $mobile 电话号码
	 * @param string $content 发送内容
	 * @param string $dstime 定时时间，为空(默认)表示立刻
	 * @param string $ext 用户自定义扩展(选填),为空(默认)需要权限
	 * @param array $msgid 客户自定义消息,为空(默认)
	 * @return object
	 */
	public function __construct($mobile='',$content='',$dstime='',$ext='',$msgid=''){
		$this->password = MD5($this->username.MD5($this->password));
		if (is_array($mobile)){
			$this->mobile=implode(',',$mobile);
		}elseif(!empty($mobile)){
			$this->mobile=$mobile;
		}
		if(!empty($dstime)){
			$this->dstime=$dstime;
		}
		if(!empty($ext)){
			$this->ext=$ext;
		}
		if (!empty($msgid)){
			$this->msgid=$msgid;
		}
		if(is_array($content)){
			foreach ($content as $k=>$v){
				$content[$k]=urlencode($v);
			}
			$this->content=implode(',',$content);
		}elseif(!empty($content)){
			$this->content =urlencode($content);	
		}	
	}
	
	
	/******数据校验并返回结果(私有)******/
	private static function check($data){
		if (is_array($data)){
			if($data['body']>0){
				$point=$data['body'];
			}else {
				switch ($data['body']){
					case  0: $point='失败';break;
					case -1: $point='用户名或者密码不正确';break;
					case -2: $point='必填选项为空';break;
					case -3: $point='短信内容0个字节';break;
					case -5: $point='余额不够';break;
					case -10: $point='用户被禁用';break;
					case -11: $point='短信内容超过500字';break;
					case -12: $point='无扩展权限（ext字段需填空）';break;
					case -13: $point='IP校验错误';break;
					case -14: $point='内容解析异常';break;
					case -990: $point='未知错误';break;
				}
			}
		}else {
			$point=$data;
		}
		return $point;
	}
	
	
	/******默认短信发送接口************/
	public static function sendOneMsg($data,$method = 'GET',$url='http://sms-cly.cn/smsSend.do',$refererUrl = '',  $contentType = 'application/x-www-form-urlencoded', $timeout = 300, $proxy = false)
	{
		$list=array();
			foreach ($data as $k=>$v)
			{
				$list[$k]=$v;
			}
		if('POST' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}elseif('GET' === strtoupper($method)){	
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}else{
			$res='失败的请求方式';
		}
		$data=self::check($res);
		return $data;
	}
	

	/*********余额查询*********/
	public static function checkBalance($data,$method = 'GET',$url='http://sms-cly.cn/balanceQuery.do',$refererUrl = '',  $contentType = 'application/x-www-form-urlencoded', $timeout = 300, $proxy = false)
	{
		$list=array();
			foreach ($data as $k=>$v)
			{
				$list[$k]=$v;
			}
		if('POST' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}elseif('GET' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}else{
			$res='失败的请求方式';
		}
		$data=self::check($res);
		return $data;
	}
	
	
	/*******修改密码*********/
	public static function upPwd($data,$newpwd,$method = 'GET',$url='http://sms-cly.cn/passwordUpdate.do',$refererUrl = '',  $contentType = 'application/x-www-form-urlencoded', $timeout = 300, $proxy = false) {
		$list=array();
		foreach ($data as $k=>$v)
		{
			$list[$k]=$v;
		}
		$list['newpassword']=$newpwd;
		if('POST' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}elseif('GET' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}else{
			$res='失败的请求方式';
		}
		$data=self::check($res);
		return $data;
	}
	
	
	/******数字签名**********/
	public static function digital($data,$plaintext,$method = 'POST',$url='http://sms-cly.cn/passwordUpdate.do',$refererUrl = '',  $contentType = 'application/x-www-form-urlencoded', $timeout = 300, $proxy = false){
		$list=array();
		foreach ($data as $k=>$v)
		{
			$list[$k]=$v;
		}
		$list['plaintext']=$plaintext;
		if('POST' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}else{
			$res='失败的请求方式';
		}
		$data=self::check($res);
		return $data;
	}
	
	
	/******个性化短信发送接口**********/
	public static function speciSendMsg($data,$method = 'POST',$url='http://sms-cly.cn/sendData.do',$refererUrl = '',  $contentType = 'application/x-www-form-urlencoded', $timeout = 300, $proxy = false)
	{
		$list=array();
		foreach ($data as $k=>$v)
		{
			$list[$k]=$v;
		}
		if('POST' === strtoupper($method)){
			$res=HTTP::send_request($url, $list, $refererUrl, $method, $contentType, $timeout, $proxy);
		}else{
			$res='失败的请求方式';
		}
		$data=self::check($res);
		return $data;
	}
}