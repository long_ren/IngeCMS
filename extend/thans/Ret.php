<?php
/**
 * @name json标准格式返回
 * Created by PhpStorm.
 * User: mycgi
 * Date: 2017/2/5
 * Time: 16:15
 */

namespace thans;

class Ret
{
    public function toJson()
    {
        return json($this->data);
    }

    private $data = array(
        'code' => 0,
        'path' => '',
        'data' => '',
        'msg'  => '',
        'url'  => '',
    );

    private static $instace = null;

    public static function i()
    {
        if (is_null(static::$instace)) {
            static::$instace = new static();
        }
        return static::$instace;
    }

    public function setData($key, $value = '')
    {
        if (is_array($key)) {
            $this->data['data'] = $key;
            return $this;
        }
        $this->data['data'][$key] = $value;
        return $this;
    }
    public function getData($key)
    {
        if (isset($this->data['data'][$key])) {
            return $this->data['data'][$key];
        }
        return null;
    }

    public function setCode($code)
    {
        $this->data['code'] = intval($code);
        $this->delReload();
        $this->delBack();
        return $this;
    }
    public function getCode()
    {
        return $this->data['code'];
    }

    public function setPath($path)
    {
        $this->data['path'] = trim((string) $path);
        return $this;
    }
    public function getPath()
    {
        return $this->data['path'];
    }

    public function setMsg($msg)
    {
        $this->data['msg'] = trim((string) $msg);
        return $this;
    }
    public function getMsg()
    {
        return $this->data['msg'];
    }

    public function setReload()
    {
        $this->data['isreload'] = 1;
        return $this;
    }
    public function setAsk($value)
    {
        $this->data['ask'] = $value;
        return $this;
    }
    public function delReload()
    {
        if (isset($this->data['isreload'])) {
            unset($this->data['isreload']);
        }
        return $this;
    }
    public function setBack($history = 1)
    {
        $this->data['back'] = $history;
        return $this;
    }
    public function delBack()
    {
        if (isset($this->data['back'])) {
            unset($this->data['back']);
        }
        return $this;
    }
    public function setUrl($value)
    {
        $this->data['url'] = $value;
        return $this;
    }
    public function getUrl()
    {
        return $this->data['url'];
    }

}
