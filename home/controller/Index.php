<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \thans\Ret;
use \think\Db;
use \think\Loader;
use \think\Session;

class Index extends Homebase
{
    public function index()
    {
        $auth = $this->user->getAuth();
        if (!$auth) {
            // die('角色不存在');
            $this->redirect(url('home/Login/index'), ['ref' => 1]);die;
        }
        $list  = $auth->getAuthRecordList();
        $menus = array();

        foreach ($list as $data) {
            $menus[] = array(
                'menu_pid'  => $data['menu_pid'],
                'id'        => $data['id'],
                'text'      => $data['name'],
                'url'       => $data['rule_value'] . '.html',
                'icon'      => empty($data['icon']) ? 'fa fa-circle-o' : $data['icon'],
                'listorder' => $data['listorder'],
            );
        }

        //对二维数组排序
        $menu_pids = array();
        foreach ($menus as $value) {
            $menu_pids[] = $value['menu_pid'];
        }
        array_multisort($menu_pids, SORT_ASC, $menus);
        //计算数组层级
        $levelArray = array();
        foreach ($menus as $value) {
            if (isset($levelArray[$value['menu_pid']])) {
                $levelArray[$value['id']] = $levelArray[$value['menu_pid']] + 1;
            } else {
                $levelArray[$value['id']] = 0;
            }
        }
        foreach ($menus as $key => $value) {
            //根据对应的层级显示  模块\控制器\方法
            if (isset($levelArray[$value['id']])) {
                $menus[$key]['targetType'] = 'iframe-tab';
            }
        }

        //对二维数组排序
        $listorders = array();
        foreach ($menus as $value) {
            $listorders[] = $value['listorder'];
        }
        array_multisort($listorders, SORT_ASC, $menus);
        $menus = node_merge($menus, 0, 'id', 'menu_pid', 'children');
        array_unshift($menus, array(
            'menu_pid'  => 0,
            'id'        => 1,
            'text'      => '主导航',
            'url'       => '',
            'icon'      => '',
            'isHeader'  => true,
            'listorder' => 0,
        ));
        // $new_menu = array();
        foreach ($menus as $key => $value) {
            if (isset($value['children']) && count($value) > 0) {
                foreach ($value['children'] as $k => $v) {
                    if (isset($v['children']) && count($v['children']) > 0) {
                        // // var_dump($menus[$key]['children'][$k]['children']);
                        // foreach ($v as $kk => $vv) {
                        //     if(isset($vv) && count($vv)>1 && is_array($vv)){
                        //         // var_dump($vv);
                        //         foreach ($vv as $cvk => $cv) {
                        //            Cache::set($cv['url'].'_menu',$menus[$key]['children'][$k]['children']);
                        //         }
                        //     }
                        // }
                        // die();
                        $menus[$key]['children'][$k]['children'] = '';

                    }
                }
            }
        }
        // die();
        $this->assign('menus', json_encode($menus));
        //系统设置

        return $this->fetch();
    }
    public function main()
    {
        $posts_count = db('posts')->where('post_type', '=', 'input_type')->count();
        $this->assign('posts_count', $posts_count);
        $member_count = db('user')->where('is_del',0)->where('user_type',1)->count();
        $this->assign('member_count', $member_count);
        // 操作日志加载
        $log = Loader::model('ActionsLog');
        // $res = $log->where('log_is_del', 0)->where('log_type', 'neq', 'GET')->order('log_id desc')->paginate(20);
        $res = $log->where('log_is_del', 0)->order('log_id desc')->paginate(20);
        // var_dump($res);
        $this->assign('log', $res);
        return $this->fetch();
    }
    public function demo()
    {
        return $this->fetch();
    }
    //日常维护
    public function system()
    {
        $action = input('action');
        switch ($action) {
            case 'trace':
                if (config('app_trace') == true) {
                    $msg = '关闭Trace';
                    $res = sys_config_setbykey('app_trace', false);
                } else {
                    $msg = '打开Trace';
                    $res = sys_config_setbykey('app_trace', true);
                }

                break;
            case 'debug':
                if (config('app_debug') == true) {
                    $msg = '关闭调试';
                    $arr = array('app_debug' => false);
                    $res = sys_config_setbyarr($arr);
                } else {
                    $msg = '打开调试';
                    $arr = array('app_debug' => true);
                    $res = sys_config_setbyarr($arr);
                }
                break;
            case 'clear_log':
                remove_dir(LOG_PATH);
                $res = true;
                $msg = '清除日志';

                break;
            case 'clear_cache':
                remove_dir(RUNTIME_PATH);
                $res = true;
                $msg = '清除缓存';
                break;
            default:

                break;
        }
        if ($res) {
            return $this->ret->setCode(0)->setMsg($msg . '成功')->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg($msg . '失败')->toJson();
        }

    }
}
