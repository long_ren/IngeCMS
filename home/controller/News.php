<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;

class News extends Homebase
{
    public function _initialize()
    {
        $this->three_menu[0]['text'] = '内容列表';
        $this->three_menu[0]['url']  = 'index';
        $this->three_menu[1]['text'] = '新增内容';
        $this->three_menu[1]['url']  = 'add';
        $this->assign('third_menu', $this->three_menu);
        $this->news = Loader::model('News');
        $this->term = Loader::model('Terms');
    }
    /**
     * 新闻列表页
     * @return [type] [description]
     */
    public function index()
    {
        $list = $this->news->where('is_del', 0)->order('new_id desc')->order('listorder asc')->paginate(10);
        $this->assign('list', $list);
        return $this->fetch();
    }
    /**
     * 编辑、添加新闻
     * @return [type] [description]
     */
    public function add()
    {
        $this->assign('menulist', $this->get_term_tree(0));
        return $this->fetch('edit');
    }
    public function go_add()
    {
        if (IS_POST) {
            $res = $this->news->saveNew(input(''));
            // var_dump($res);
            if ($res['code']) {
                return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'].$res['error_log'])->toJson();
            }
        }
    }
    public function edit()
    {
        $id = input('id');
        if ($id) {
            $data     = $this->news->where('new_id', input('id'))->find();
            $new_flag = json_decode($data['new_flag'], true);
            $this->assign('new_flag', $new_flag);
            $this->assign('data', $data);
            $term   = $this->term->getTermForObjectId($id);
            $select = $term['term_id'];
            $this->assign('menulist', $this->get_term_tree($select));
            return $this->fetch();
        }
    }
    public function go_edit()
    {
        if (IS_POST) {
            $res = $this->news->saveNew(input(''), input('id'));
            // var_dump($res);
            if ($res['code']) {
                return $this->ret->setCode(0)->setMsg("保存成功")->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
            }
        }
    }
    public function get_term_tree($select = 0)
    {
        if (!$select) {
            $select = 0;
        }
        $list = $this->term->select();
        $tree = $foo = new \thans\Common\tree($list);
        $tree->formatArrayForTree('term_id', 'term_pid', 'name');
        $data = $tree->get_tree(0, "<option value='\$id' \$selected>\$spacer \$name </option>", $select);
        return $data;
    }
    
    public function del()
    {
        $res = $this->news->delNew(input('id'));
        if ($res) {
            return $this->ret->setCode(0)->setMsg('删除成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('删除失败')->toJson();
        }
    }
    public function listorder()
    {
        $res = listorder('new_id', input('id/a'), input('order/a'), $this->news);
        if ($res) {
            return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
        }
    }
    public function change_status()
    {
        $id                 = input('id');
        $data['new_status'] = input('status');
        $res                = $this->news->where('new_id', $id)->update($data);
        if ($res) {
            if (input('status') == 0) {
                $status = "未审";
            } else {
                $status = "已审";
            }
            return $this->ret->setCode(0)->setMsg($status)->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('修改状态失败')->toJson();
        }
    }
}
