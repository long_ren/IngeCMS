<?php
namespace app\home\controller;

use \app\common\controller\Homebase;
use \think\Loader;

class Term extends Homebase
{
    public function _initialize()
    {
        $this->three_menu[0]['text'] = '分类列表';
        $this->three_menu[0]['url']  = 'index';
        $this->three_menu[1]['text'] = '新增分类';
        $this->three_menu[1]['url']  = 'add';
        $this->assign('third_menu', $this->three_menu);
        $this->term = Loader::model('Terms');
    }
    public function index()
    {
        $list = $this->term->order('listorder ASC')->select();

        $tree = new \thans\Common\tree($list);
        $list = $tree->formatArrayForTree('term_id', 'term_pid', 'name');
        //对二维数组排序
        $menu_pids = array();
        foreach ($list as $value) {
            $menu_pids[] = $value['term_pid'];
        }
        // array_multisort($menu_pids, SORT_ASC, $list);

        //计算数组层级
        $levelArray = array();
        foreach ($list as $value) {
            if (isset($levelArray[$value['term_pid']])) {
                $levelArray[$value['id']] = $levelArray[$value['term_pid']] + 1;
            } else {
                $levelArray[$value['id']] = 0;
            }
        }

        foreach ($list as $key => $value) {
            //根据对应的层级显示  模块\控制器\方法
            $list[$key]['action'] = '<a href="' . url('edit', 'id=' . $list[$key]['id']) . '"><i class="fa fa-pencil green control"></i></a>&nbsp;<i class="fa fa-trash-o red control confirm-rst-url-btn" data-url="' . url('del', 'id=' . $list[$key]['id']) . '" data-info="您确定要删除本菜单吗？" title="删除"></i>';
        }
        $tree->initArr($list);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│ ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $str        = "<tr><td class='pxinput'><input type='hidden' name='id[]' value='\$id'><input type='text' name='order[]' value='\$listorder' class='pxinput'></td>
        <td>\$id</td>
        <td>\$spacer\$name</td>
        <td>\$action</td></tr>";
        $list = $tree->get_tree(0, $str);
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function add()
    {
        $this->assign('menulist', $this->get_term_tree(0));
        return $this->fetch('edit');
    }
    public function edit($value='')
    {
        $id = input('id');
        if ($id) {
            $data = $this->term->where('term_id', input('id'))->find();
            $this->assign('data', $data);
            $select = $data['term_pid'];
            $this->assign('menulist', $this->get_term_tree($select));
            return $this->fetch('edit');
        }
        
    }
    public function go_add()
    {
        if (IS_POST) {
            $res = $this->term->saveTerms(input(''));
            if ($res) {
                return $this->ret->setCode(0)->setMsg($res['msg'])->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
            }
        }
    }
    public function go_edit()
    {
        $id = input('id');
        if (IS_POST) {
            $res = $this->term->saveTerms(input(''), $id);
            if ($res) {
                return $this->ret->setCode(0)->setMsg($res['msg'])->setAsk("保存成功,是否继续？")->toJson();
            } else {
                return $this->ret->setCode(1)->setMsg($res['msg'])->toJson();
            }
        }
    }
    public function get_term_tree($select = 0)
    {
        if (!$select) {
            $select = 0;
        }
        $list = $this->term->select();
        $tree = $foo = new \thans\Common\tree($list);
        $tree->formatArrayForTree('term_id', 'term_pid', 'name');
        $data = $tree->get_tree(0, "<option value='\$id' \$selected>\$spacer \$name </option>", $select);
        return $data;
    }
    public function del()
    {
        $recycleBin     = Loader::model('RecycleBin');
        $map['term_id'] = input('id');
        $data           = $this->term->where($map)->find();
        $res            = $recycleBin->cutout($this->term, $data, $map);
        if ($res) {
            return $this->ret->setMsg('删除成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('删除失败')->toJson();
        }
    }
    public function listorder()
    {
        $res = listorder('term_id', input('id/a'), input('order/a'), $this->term);
        if ($res) {
            return $this->ret->setCode(0)->setMsg('排序更新成功')->setReload()->toJson();
        } else {
            return $this->ret->setCode(1)->setMsg('排序更新失败')->toJson();
        }
    }
}
