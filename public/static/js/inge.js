$(document).ready(function() {
    /**
     * 通用AJAX提交
     * @param  {[type]}   [description]
     * @param  {[type]}   [description]
     * @return {[type]}   [description]
     */
    $('.ingeAjax').submit(function() {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(), // 要提交的表单
            success: function(msg) {
                // console.log(msg);
                /**
                 * 返回数据格式为json 包含以下字段 msg（提示信息） code（错误代码 默认0错误 1成功）
                 * isreload（是否刷新）url（成功后跳转） errorurl（失败后跳转）
                 */
                if (msg.code == '0' || msg.code == 0) {
                    if (msg.ask !== undefined && msg.ask != "") {
                        layer.confirm(msg.ask, {
                            btn: ['继续', '返回'],
                            closeBtn: 0,
                            title: '询问',
                        }, function() {
                            window.setTimeout("window.location.reload();", 100);
                        }, function() {
                            window.setTimeout("window.history.go(-1);", 100);
                        });
                    } else {
                        layer.msg(msg.msg, {
                            icon: 6,
                        });
                    }
                } else {
                    layer.alert(msg.msg, {
                        icon: 5,
                        title: '提示',
                    });
                }

                if (msg.url !== undefined && msg.url != "") {
                    window.setTimeout("window.location = '" + msg.url + "';", 600);
                }
                if (msg.back >= '1') {
                    window.setTimeout("window.history.go(-" + msg.back + ");", 600);
                }
                if (msg.isreload == '1') {
                    window.setTimeout("window.location.reload();", 600);
                }
            },
            error: function(error) {
                layer.msg("网络错误");
            }
        });
        return false;
    });
    /**
     * 通用ajax函数
     */
    function inge_ajax($url, $data, $type, $success_fun = '', $error_fun = '') {
        $.ajax({
            type: $type,
            url: $url,
            data: $data, // 要提交的数据
            success: function(msg) {
                /**
                 * 返回数据格式为json 包含以下字段 msg（提示信息） code（错误代码 默认0错误 1成功）
                 * isreload（是否刷新）url（成功后跳转） errorurl（失败后跳转） 优先级 先url,后回退,最后刷新
                 */
                if (msg.code == '0' || msg.code == 0) {
                    layer.msg(msg.msg, {
                        icon: 6,
                    });
                    if ($success_fun != '') {
                        $success_fun();

                    }
                } else {
                    layer.msg(msg.msg);
                    if ($success_fun != '') {
                        $error_fun();
                    }
                }
                if (msg.url !== undefined && msg.url != "") {
                    console.log('tz');
                    window.setTimeout("window.location = '" + msg.url + "';", 600);
                }
                if (msg.back >= '1') {
                    console.log('back');
                    window.setTimeout("window.history.go(-" + msg.back + ");", 600);
                }
                console.log(msg.isreload);
                if (msg.isreload == '1') {
                    window.setTimeout("window.location.reload();", 600);
                }
            },
            error: function(error) {
                console.log($url);
                console.log(error);
                layer.msg("网络错误");
            }
        });
        return false;
    }

    /**
     *确认跳转链接按钮
     */
    $('.confirm-rst-url-btn').on('click', function(event) {
        var url = $(this).attr('data-url');
        layer.confirm($(this).attr('data-info'), {
            btn: ['确认', '取消'], //按钮
            title: '提示'
        }, function() {
            inge_ajax(url);
        }, function() {
            layer.msg('已取消', {
                icon: 1
            });
        });
    });
    /**
     * ajax提交后提示 打开或关闭状态
     */
    $('.url-btn-msg-open-close').on('click', function(event) {
        var url = $(this).attr('data-url');
        var that = $(this);
        inge_ajax(url, '', 'GET', function() {
            if (that.html() == "打开") {
                that.text('关闭');
            } else {
                that.text('打开');
            }
        });
    });
    /**
     * ajax提交后提示 无状态修改
     */
    $('.url-btn-msg').on('click', function(event) {
        var url = $(this).attr('data-url');
        inge_ajax(url);
    });
    /**
     * 状态修改 status
     */
    $('.status').on('click', function() {
        var ob = $(this);
        if (ob.hasClass('label-success')) {
            var url = $(this).attr('data-href') + '?&status=0';
        } else {
            var url = $(this).attr('data-href') + '?&status=1';
        }
        $.ajax({
            type: 'GET',
            url: url,
            success: function(msg) {
                if (msg.code == '0' || msg.code == 0) {
                    console.log('1');
                    if (ob.hasClass('label-success')) {
                        console.log('2');
                        ob.removeClass('label-success');
                        ob.addClass('label-danger');
                        ob.html(msg.msg);
                    } else {
                        console.log('3');
                        ob.removeClass('label-danger');
                        ob.addClass('label-success');
                        ob.html(msg.msg);
                    }
                } else {
                    layer.msg(msg.msg);
                }
            },
            error: function(error) {
                layer.msg("网络错误");
            }
        });
    });
    /*
     ** randomWord 产生任意长度随机字母数字组合
     ** randomFlag-是否任意长度 min-任意长度最小位[固定位数] max-任意长度最大位
     ** xuanfeng 2014-08-28
     */

    function inge_get_random_str(randomFlag, min, max) {
        var str = "",
            range = min,
            arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        // 随机产生
        if (randomFlag) {
            range = Math.round(Math.random() * (max - min)) + min;
        }
        for (var i = 0; i < range; i++) {
            pos = Math.round(Math.random() * (arr.length - 1));
            str += arr[pos];
        }
        return str;
    }
    //单行展开
    $('.trdetail').on('click', function() {
        if ($(this).next().css('display') == 'table-row') {
            $(this).next().css('display', 'none');
        } else {
            $(this).next().css('display', 'table-row');
        }
    });

});
$(document).ajaxStart(function() {
    Pace.restart();
});